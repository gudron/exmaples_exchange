package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"strings"

	cmds "github.com/bfg-dev/crypto-core/cmd"
	"github.com/bfg-dev/crypto-core/pkg/helpers/cmd"
)

var (
	config  string
	cmdName string
)

func toCapital(s string) string {
	if len(s) < 2 {
		return s
	}
	return strings.ToUpper(s[0:1]) + strings.ToLower(s[1:])
}

func whatAmI() string {
	return filepath.Base(os.Args[0])
}

func callCmdFunc(name string) error {

	var args []reflect.Value

	if name == "Main" || name == "Core" || name == "Crypto-core" {
		return fmt.Errorf("Sorry, i can not work without cmd name, you can set it via '-cmd' option or via soft link")
	}

	m := reflect.ValueOf(&cmds.Cmd{}).MethodByName(name)
	if !m.IsValid() {
		return fmt.Errorf("Entrypoint for %v not found", name)
	}

	switch m.Kind() {
	case reflect.Func:
		m.Call(args)
	default:
		return fmt.Errorf("Entrypoint for %v is not valid", name)
	}

	return fmt.Errorf("Unknown error")
}

func main() {
	defaultCmdName := whatAmI()

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	cmd.DieIfError(err, "Unable to get current path", nil)

	defaultConfigPath := path.Join(dir, defaultCmdName+".toml")

	flag.StringVar(&config, "config", defaultConfigPath, "You can set config file path")
	flag.StringVar(&cmdName, "cmd", defaultCmdName, "You can set cmd name")
	flag.Parse()

	c := cmds.Cmd{}
	c.SetConfig(config)

	cmd.DieIfError(callCmdFunc(toCapital(cmdName)), "Unable to run cmd", nil)
}
