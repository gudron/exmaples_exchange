package mocks

import (
	"github.com/stretchr/testify/mock"
	"go.uber.org/zap/zapcore"
)

type ZapLogger struct {
	mock.Mock
}

func (m *ZapLogger) Info(msg string, fields ...zapcore.Field) {
	m.Called(msg, fields)
}

func (m *ZapLogger) Error(msg string, fields ...zapcore.Field) {
	m.Called(msg, fields)
}
