#!/bin/bash
set -e

envsubst < /etc/cron.d/crypto-core > /etc/cron.d/crypto-core 

stop_cron_daemon() {
    local cron_pid="$1";
    if [ -n "$cron_pid" ] && kill -0 "$cron_pid" &>/dev/null; then
        kill "$cron_pid";
    else
        exit 0
    fi
}

/usr/bin/crontab -u root /etc/cron.d/crypto-core > /dev/null

/usr/sbin/cron -f -L15 &
cron_pid=$!

trap "stop_cron_daemon $cron_pid" INT TERM EXIT

wait $cron_pid