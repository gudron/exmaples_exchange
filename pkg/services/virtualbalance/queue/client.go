package queue

import (
	"context"

	amqpHelper "github.com/bfg-dev/crypto-core/pkg/helpers/amqp"
	"github.com/bfg-dev/crypto-core/pkg/services/amqp"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/bfg-dev/crypto-core/pkg/types/merk"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"go.uber.org/zap"
)

type client struct {
	exchangeName           types.AmqpExchangeName
	amqpConnectionProvider amqp.ConnectionProvider
	logger                 *zap.Logger
}

func (c *client) PublishSyncMessage(ctx context.Context, bm BalanceSyncMessage) error {
	conn, err := c.amqpConnectionProvider.GetConnection()

	if err != nil {
		return errors.Wrap(err, "queue.client.PublishSyncMessage, unable to get amqp connection")
	}
	defer conn.Close()

	channel, err := conn.Channel()

	if err != nil {
		return errors.Wrap(err, "queue.client.PublishSyncMessage, unable to get amqp channel")
	}
	defer channel.Close()

	err = channel.ExchangeDeclare(string(c.exchangeName), "topic", true, false, false, false, nil)

	if err != nil {
		return errors.Wrap(err, "queue.client.PublishSyncMessage, unable to declare exchange")
	}

	jsonPublisher, err := amqpHelper.NewJsonPublisher(bm)

	if err != nil {
		return errors.Wrap(err, "queue.client.PublishSyncMessage, unable to create json publisher ")
	}

	jsonPublisher.AddHeaders("RequestID", uuid.NewV4().String())
	msg, err := jsonPublisher.Encode()

	if err != nil {
		return errors.Wrap(err, "queue.client.PublishSyncMessage, unable to build amqp message")
	}

	return channel.Publish(string(c.exchangeName), string(merk.VbBalanceSync), false, false, *msg)
}

func (c *client) PublishCommitMessage(ctx context.Context, operationID int64) error {
	conn, err := c.amqpConnectionProvider.GetConnection()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishCommitMessage, unable to get amqp connection")
	}
	defer conn.Close()

	channel, err := conn.Channel()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishCommitMessage, unable to get amqp channel")
	}
	defer channel.Close()

	err = channel.ExchangeDeclare(string(c.exchangeName), "topic", true, false, false, false, nil)

	if err != nil {
		return errors.Wrap(err, "queue.client.publishCommitMessage, unable to declare exchange")
	}

	message := &OperationMessage{
		OperationID: operationID,
		RetryMessage: RetryMessage{
			RetryCount: 0,
		},
	}

	jsonPublisher, err := amqpHelper.NewJsonPublisher(message)

	if err != nil {
		return errors.Wrap(err, "queue.client.publishCommitMessage, unable to create json publisher ")
	}

	jsonPublisher.AddHeaders("RequestID", uuid.NewV4().String())
	msg, err := jsonPublisher.Encode()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishCommitMessage, unable to build amqp message")
	}

	return channel.Publish(string(c.exchangeName), string(merk.VbOperationCommit), false, false, *msg)
}

func (c *client) PublishPrepareMessage(ctx context.Context, operationID int64) error {
	conn, err := c.amqpConnectionProvider.GetConnection()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishPrepareMessage, unable to get amqp connection")
	}
	defer conn.Close()

	channel, err := conn.Channel()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishPrepareMessage, unable to get amqp channel")
	}
	defer channel.Close()

	err = channel.ExchangeDeclare(string(c.exchangeName), "topic", true, false, false, false, nil)

	if err != nil {
		return errors.Wrap(err, "queue.client.publishPrepareMessage, unable to declare exchange")
	}

	message := &OperationMessage{
		OperationID: operationID,
		RetryMessage: RetryMessage{
			RetryCount: 0,
		},
	}

	jsonPublisher, err := amqpHelper.NewJsonPublisher(message)

	if err != nil {
		return errors.Wrap(err, "queue.client.publishPrepareMessage, unable to create json publisher ")
	}

	jsonPublisher.AddHeaders("RequestID", uuid.NewV4().String())
	msg, err := jsonPublisher.Encode()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishPrepareMessage, unable to build amqp message")
	}

	return channel.Publish(string(c.exchangeName), string(merk.VbOperationPrepare), false, false, *msg)
}

func (c *client) PublishRollbackMessage(ctx context.Context, operationID int64) error {
	conn, err := c.amqpConnectionProvider.GetConnection()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishRollbackMessage, unable to get amqp connection")
	}
	defer conn.Close()

	channel, err := conn.Channel()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishRollbackMessage, unable to get amqp channel")
	}
	defer channel.Close()

	err = channel.ExchangeDeclare(string(c.exchangeName), "topic", true, false, false, false, nil)

	if err != nil {
		return errors.Wrap(err, "queue.client.publishRollbackMessage, unable to declare exchange")
	}

	message := &OperationMessage{
		OperationID: operationID,
		RetryMessage: RetryMessage{
			RetryCount: 0,
		},
	}

	jsonPublisher, err := amqpHelper.NewJsonPublisher(message)

	if err != nil {
		return errors.Wrap(err, "queue.client.publishRollbackMessage, unable to create json publisher ")
	}

	jsonPublisher.AddHeaders("RequestID", uuid.NewV4().String())
	msg, err := jsonPublisher.Encode()

	if err != nil {
		return errors.Wrap(err, "queue.client.publishRollbackMessage, unable to build amqp message")
	}

	return channel.Publish(string(c.exchangeName), string(merk.VbOperationRollback), false, false, *msg)
}

func (c *client) PublishConvertUserAssetMessage(ctx context.Context, cm ConvertUserAssetMessage) error {

	conn, err := c.amqpConnectionProvider.GetConnection()
	if err != nil {
		return errors.Wrap(err, "queue.client.PublishConvertUserAssetMessage, unable to get amqp connection")
	}
	defer conn.Close()

	channel, err := conn.Channel()
	if err != nil {
		return errors.Wrap(err, "queue.client.PublishConvertUserAssetMessage, unable to get amqp channel")
	}
	defer channel.Close()

	err = channel.ExchangeDeclare(string(c.exchangeName), "topic", true, false, false, false, nil)
	if err != nil {
		return errors.Wrap(err, "queue.client.PublishConvertUserAssetMessage, unable to declare exchange")
	}

	jsonPublisher, err := amqpHelper.NewJsonPublisher(cm)
	if err != nil {
		return errors.Wrap(err, "queue.client.PublishConvertUserAssetMessage, unable to create json publisher ")
	}

	jsonPublisher.AddHeaders("RequestID", uuid.NewV4().String())
	msg, err := jsonPublisher.Encode()
	if err != nil {
		return errors.Wrap(err, "queue.client.PublishConvertUserAssetMessage, unable to build amqp message")
	}

	return channel.Publish(string(c.exchangeName), string(merk.VbConvertUserAsset), false, false, *msg)
}

func NewClient(
	exchangeName types.AmqpExchangeName,
	amqpConnectionProvider amqp.ConnectionProvider,
) (VirtualBalanceQueueClient, error) {

	if exchangeName == "" {
		return nil, errors.New("queue.NewClient, exchangeName must be not empty")
	}

	if amqpConnectionProvider == nil {
		return nil, errors.New("queue.NewClient, amqpConnectionProvider must be not empty")
	}

	return &client{
		exchangeName:           exchangeName,
		amqpConnectionProvider: amqpConnectionProvider,
	}, nil
}
