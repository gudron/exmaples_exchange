package queue

import (
	"context"

	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	vb "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
)

type OperationPrepareListener interface {
	Listen(handler OperationHandlers) error
}

type OperationRollbackListener interface {
	Listen(handler OperationHandlers) error
}

type OperationCommitListener interface {
	Listen(handler OperationHandlers) error
}

type ConvertUserAssetListener interface {
	Listen(handler ConvertUserAssetHandler) error
}

type BalanceSyncListener interface {
	Listen(handler BalanceSyncHandler) error
}

type OperationHandlers interface {
	HandlePrepareOperation(OperationMessage) virtualbalance.Decision
	HandleRollbackOperation(OperationMessage) virtualbalance.Decision
	HandleCommitOperation(OperationMessage) virtualbalance.Decision
}

type ConvertUserAssetHandler interface {
	HandleConvertUserAsset(ConvertUserAssetMessage) vb.Decision
}

type BalanceSyncHandler interface {
	HandleBalanceSync(BalanceSyncMessage) error
}

type VirtualBalanceQueueClient interface {
	PublishCommitMessage(context.Context, int64) error
	PublishRollbackMessage(context.Context, int64) error
	PublishPrepareMessage(context.Context, int64) error
	PublishSyncMessage(context.Context, BalanceSyncMessage) error
	PublishConvertUserAssetMessage(context.Context, ConvertUserAssetMessage) error
}

const retryLimit = 10
