package queue

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	amqpHelper "github.com/bfg-dev/crypto-core/pkg/helpers/amqp"
	servicesAmqp "github.com/bfg-dev/crypto-core/pkg/services/amqp"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/decision"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

type operationPrepareListener struct {
	logger                 *zap.Logger
	exchangeName           types.AmqpExchangeName
	routingKey             types.AmqpRoutingKey
	concurrency            int
	amqpConnectionProvider servicesAmqp.ConnectionProvider
	handler                OperationHandlers
	maxRetriesCount        int
	uniq                   string
	queueName              string
	queueClient            VirtualBalanceQueueClient
}

func (l *operationPrepareListener) Listen(handler OperationHandlers) error {
	l.handler = handler

	conn, err := l.amqpConnectionProvider.GetConnection()
	if err != nil {
		return errors.Wrap(err, "operationPrepareListener.Listen, unable to get amqp connection")
	}

	err = l.initListener(conn)
	if err != nil {
		return errors.Wrap(err, "operationPrepareListener.Listen, unable to init listener")
	}

	return nil
}

func (l *operationPrepareListener) listen(msgs <-chan amqp.Delivery, handler OperationHandlers) {
	l.logger.Debug("operationPrepareListener.Listen, listening started")

	var concurrencyChan = make(chan int, l.concurrency)
	for msg := range msgs {
		l.logger.Debug("operationPrepareListener.Listen, received message", zap.Any("message", msg))
		concurrencyChan <- 1
		go l.msgProcessor(concurrencyChan, msg, handler)
	}

	l.logger.Debug("operationPrepareListener.Listen, processing stopped")
}

func (l *operationPrepareListener) msgProcessor(channel chan int, message amqp.Delivery, handler OperationHandlers) {
	defer func() {
		<-channel
	}()

	var m OperationMessage

	err := json.Unmarshal(message.Body, &m)
	if err != nil {
		l.logger.Error("operationPrepareListener", zap.String("body", string(message.Body)), zap.Error(err))

		message.Ack(false)
		return
	}

	l.logger.Debug("operationPrepareListener.msgProcessor, received operation prepare message", zap.Any("body", m))

	dec := handler.HandlePrepareOperation(m)

	switch dec {
	case decision.Cancel:
		err := l.queueClient.PublishRollbackMessage(context.TODO(), m.OperationID)
		if err != nil {
			l.logger.Error("operationPrepareListener.HandlePrepareOperation, PublishRollbackMessage error",
				zap.Int64("operationID", m.OperationID))
		}
	case decision.Continue:
		err := l.queueClient.PublishCommitMessage(context.TODO(), m.OperationID)
		if err != nil {
			l.logger.Error("operationPrepareListener.HandlePrepareOperation, publishCommitMessage error.",
				zap.Int64("operationID", m.OperationID))
		}
	case decision.Repeat:
		l.logger.Info("operationPrepareListener.HandlePrepareOperation, message retry.",
			zap.Int64("operationID", m.OperationID),
			zap.Any("retryCount", m.RetryCount),
		)
		m.RetryCount++

		if m.RetryCount > retryLimit {
			message.Reject(false)

			l.logger.Error("Retry count exceeded. Message rejected.",
				zap.Int64("OperationID", m.OperationID))
		} else {
			time.Sleep(2 * time.Second)

			err = l.republish(m)

			if err != nil {
				message.Nack(false, true)

				l.logger.Error("operationPrepareListener.msgProcessor, republish", zap.Error(err))
			} else {
				message.Ack(false)
			}
		}

		return

	}

	message.Ack(false)

	l.logger.Debug("operationPrepareListener.msgProcessor, message successfully processed",
		zap.Any("message", message),
		zap.Int64("operationID", m.OperationID),
	)
}

func (l *operationPrepareListener) initListener(conn *amqp.Connection) error {
	ch, err := conn.Channel()
	if err != nil {
		ch.Close()
		return errors.Wrap(err, "operationPrepareListener.initListener, unable to create amqp channel")
	}

	err = ch.ExchangeDeclare(string(l.exchangeName), "topic", true, false, false, false, nil)
	if err != nil {
		ch.Close()
		return errors.Wrap(err, "operationPrepareListener.initListener, unable to declare exchange")
	}

	deadExchange := amqpHelper.BuildDeadExchangeName(l.exchangeName)

	err = ch.ExchangeDeclare(deadExchange, "fanout", true, false, false, false, nil)
	if err != nil {
		ch.Close()
		return errors.Wrap(err, "operationPrepareListener.initListener, unable to declare dead exchange")
	}

	queue, err := ch.QueueDeclare(amqpHelper.BuildUniqName(l.queueName, l.uniq), true, false, false, false, amqp.Table{
		"x-dead-letter-exchange": deadExchange,
	})
	if err != nil {
		ch.Close()
		return errors.Wrap(err, "operationPrepareListener.initListener, unable to declare queue")
	}

	err = ch.QueueBind(queue.Name, string(l.routingKey), string(l.exchangeName), false, nil)
	if err != nil {
		ch.Close()
		return errors.Wrap(err, "operationPrepareListener.initListener, unable to bind queue to exchange")
	}

	msgs, err := ch.Consume(queue.Name, "operationPrepareListener", false, false, false, false, nil)
	if err != nil {
		ch.Close()
		return errors.Wrap(err, "operationPrepareListener.initListener, unable to create queue consumer")
	}

	notifyCloseChannel := make(chan *amqp.Error)
	ch.NotifyClose(notifyCloseChannel)

	go l.listen(msgs, l.handler)

	go func() {
		notifyCloseErr := <-notifyCloseChannel
		l.logger.Error("operationPrepareListener.initListener, amqp channel was closed",
			zap.Int("code", notifyCloseErr.Code),
			zap.Bool("server", notifyCloseErr.Server),
			zap.String("reason", notifyCloseErr.Reason),
			zap.Bool("recover", notifyCloseErr.Recover))

		l.amqpConnectionProvider.ProvideConnectionToCb(l.initListener, "operationPrepareListener", int64(l.maxRetriesCount))
	}()

	return nil
}

func (l *operationPrepareListener) republish(m interface{}) error {
	conn, err := l.amqpConnectionProvider.GetConnection()
	if err != nil {
		return errors.Wrap(err, "operationPrepareListener.republish, unable to get amqp connection")
	}
	defer conn.Close()

	channel, err := conn.Channel()
	if err != nil {
		return errors.Wrap(err, "operationPrepareListener.republish, unable to get amqp channel")
	}
	defer channel.Close()

	jsonPublisher, err := amqpHelper.NewJsonPublisher(m)
	if err != nil {
		return errors.Wrap(err, "operationPrepareListener.republish, unable to create json publisher ")
	}

	jsonPublisher.AddHeaders("RequestID", uuid.NewV4().String())
	msg, err := jsonPublisher.Encode()
	if err != nil {
		return errors.Wrap(err, "operationPrepareListener.republish, unable to build amqp message")
	}

	return channel.Publish(string(l.exchangeName), string(l.routingKey), false, false, *msg)
}

// NewOperationPrepareListener ...
func NewOperationPrepareListener(
	exchangeName types.AmqpExchangeName,
	routingKey types.AmqpRoutingKey,
	queueName string,
	amqpConnectionProvider servicesAmqp.ConnectionProvider,
	concurrency int,
	maxRetriesCount int,
	uniq string,
	vbQueueClient VirtualBalanceQueueClient,
	logger *zap.Logger) (OperationPrepareListener, error) {

	const maxConcurrency = 1000
	const defaultConcurrency = 1

	if vbQueueClient == nil {
		return nil, errors.New("amqp.NewOperationPrepareListener, VirtualBalanceQueueClient must be not empty")
	}

	if amqpConnectionProvider == nil {
		return nil, errors.New("amqp.NewOperationPrepareListener, amqpConnectionProvider must be not empty")
	}

	if logger == nil {
		return nil, errors.New("amqp.NewOperationPrepareListener, logger must be not empty")
	}

	listener := operationPrepareListener{
		amqpConnectionProvider: amqpConnectionProvider,
		exchangeName:           exchangeName,
		routingKey:             routingKey,
		logger:                 logger,
		maxRetriesCount:        maxRetriesCount,
		uniq:                   uniq,
		queueName:              queueName,
		queueClient:            vbQueueClient,
	}

	if concurrency > maxConcurrency {
		return nil, errors.New(fmt.Sprintf("amqp.NewOperationPrepareListener, concurrency must be between 0 and %v", maxConcurrency))
	}

	listener.concurrency = concurrency

	if concurrency == 0 {
		listener.concurrency = defaultConcurrency
	}

	return &listener, nil
}
