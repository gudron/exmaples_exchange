package queue

import (
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/decision"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operations"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type operationHandlers struct {
	transactionsStrategy transactions.Strategy
	operationsRepository operations.Repo
	balanceService       balance.Service
	logger               *zap.Logger
}

func (s *operationHandlers) HandlePrepareOperation(m OperationMessage) virtualbalance.Decision {

	s.logger.Debug("HandlePrepareOperation, message successfully received",
		zap.Any("message", m),
	)

	o, err := s.operationsRepository.Get(m.OperationID)
	if err != nil {
		s.logger.Error("HandlePrepareOperation, operationsRepository.Get error",
			zap.Int64("OperationID", m.OperationID),
			zap.Error(err),
		)
		return decision.Repeat
	}

	list, err := s.transactionsStrategy.GetList(o.Type, &o.Currency)
	if err != nil {
		s.logger.Error("HandlePrepareOperation, operationStrategy.GetList error",
			zap.String("OperationType", string(o.Type)),
			zap.Error(err),
		)
		return decision.Repeat
	}

	operation := operations.New(s.operationsRepository, list, s.balanceService, s.logger)

	d := operation.Prepare(m.OperationID)

	s.logger.Debug("HandlePrepareOperation, operation prepared",
		zap.Any("decision", decision.String(d)),
		zap.Int64("operationID", o.ID),
		zap.String("OperationType", string(o.Type)),
		zap.Any("list", list),
	)

	return d
}

func (s *operationHandlers) HandleCommitOperation(m OperationMessage) virtualbalance.Decision {

	s.logger.Debug("HandleCommitOperation, message successfully received",
		zap.Any("message", m),
	)

	o, err := s.operationsRepository.Get(m.OperationID)

	if err != nil {
		s.logger.Error("HandleCommitOperation, operationsRepository.Get error",
			zap.Int64("OperationID", m.OperationID))
		return decision.Repeat
	}

	list, err := s.transactionsStrategy.GetList(o.Type, &o.Currency)
	if err != nil {
		s.logger.Error("HandleCommitOperation, operationStrategy.GetList error",
			zap.String("OperationType", string(o.Type)))
		return decision.Repeat
	}

	operations := operations.New(s.operationsRepository, list, s.balanceService, s.logger)

	d := operations.Commit(m.OperationID)

	s.logger.Debug("HandleCommitOperation, operation committed",
		zap.Any("decision", decision.String(d)),
		zap.Int64("operationID", o.ID),
		zap.String("OperationType", string(o.Type)),
		zap.Any("list", list),
	)

	return d
}

func (s *operationHandlers) HandleRollbackOperation(m OperationMessage) virtualbalance.Decision {

	s.logger.Debug("HandleRollbackOperation, message successfully received",
		zap.Any("message", m),
	)

	o, err := s.operationsRepository.Get(m.OperationID)

	if err != nil {
		s.logger.Error("HandleRollbackOperation, operationsRepository.Get error",
			zap.Int64("OperationID", m.OperationID))
		return decision.Repeat
	}

	list, err := s.transactionsStrategy.GetList(o.Type, &o.Currency)
	if err != nil {
		s.logger.Error("HandleRollbackOperation, operationStrategy.GetList error",
			zap.String("OperationType", string(o.Type)))
		return decision.Repeat
	}

	operations := operations.New(s.operationsRepository, list, s.balanceService, s.logger)

	d := operations.Rollback(m.OperationID)

	s.logger.Debug("HandleRollbackOperation, operation rolled back",
		zap.Any("decision", decision.String(d)),
		zap.Int64("operationID", o.ID),
		zap.String("OperationType", string(o.Type)),
		zap.Any("list", list),
	)

	return d
}

// NewOperationHandlers ...
func NewOperationHandlers(
	transactionsStrategy transactions.Strategy,
	operationsRepository operations.Repo,
	balanceService balance.Service,
	logger *zap.Logger,
) (OperationHandlers, error) {
	if transactionsStrategy == nil {
		return nil, errors.New("virtualbalance.NewOperationHandlers transactionsStrategy cannot be empty")
	}

	if operationsRepository == nil {
		return nil, errors.New("virtualbalance.NewOperationHandlers operationsRepository cannot be empty")
	}

	if balanceService == nil {
		return nil, errors.New("virtualbalance.NewOperationHandlers balanceService cannot be empty")
	}

	if logger == nil {
		return nil, errors.New("virtualbalance.NewOperationHandlers logger cannot be empty")
	}

	return &operationHandlers{
		transactionsStrategy: transactionsStrategy,
		operationsRepository: operationsRepository,
		balanceService:       balanceService,
		logger:               logger,
	}, nil
}
