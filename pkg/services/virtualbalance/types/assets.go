package types

import (
	"strings"

	vb "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/shopspring/decimal"
)

const (
	SpotwareValueDecimals  = 1e8
	SpotwareVolumeDecimals = 1e8
)

const (
	USD      vb.Asset = "USD"
	EUR      vb.Asset = "EUR"
	GBP      vb.Asset = "GBP"
	BTC      vb.Asset = "BTC"      //Bitcoin
	ETH      vb.Asset = "ETH"      //Ethereum
	LTC      vb.Asset = "LTC"      //Litecoin
	BCH      vb.Asset = "BCH"      //BitcoinCash
	XRP      vb.Asset = "XRP"      //Ripple
	BTG      vb.Asset = "BTG"      //BitcoinGold
	BMxiETH  vb.Asset = "BMXIETH"  //Inverse ETH
	BMxTMR   vb.Asset = "BMXTMR"   //Top 20 Price Mean Reversion Strategy with Trend Protection
	BMxVTP   vb.Asset = "BMXVTP"   //One Day Most Volatile Strategy with Trend Protection
	BMx1DV   vb.Asset = "BMX1DV"   //One Day Most Volatile Strategy
	BMxDDR   vb.Asset = "BMXDDR"   //Daily Delta Rebalancing Strategy
	BMxBN20  vb.Asset = "BMXBN20"  //Binance Top 20 Most Traded Exchange Traded Index
	BMxT10   vb.Asset = "BMXT10"   //t10
	BMxT20   vb.Asset = "BMXT20"   //t20
	BMxSPY   vb.Asset = "BMXSPY"   //S&P 500
	BMxPMR   vb.Asset = "BMXPMR"   //pmr
	BMxEEM   vb.Asset = "BMXEEM"   //eem
	BMxXMI   vb.Asset = "BMXXMI"   //xmi
	BMxWTR   vb.Asset = "BMXWTR"   //wtr
	BookLYFT vb.Asset = "BOOKLYFT" //lyft
	BMxEGM   vb.Asset = "BMXEGM"   //egm
	BMxBRXT  vb.Asset = "BMXBRXT"  //brxt
	BMxLYFT  vb.Asset = "BMXLYFT"  //lyft
	BMxDT10  vb.Asset = "BMXDT10"  //Sentiment Top 10 ETx (Daneel.i0)
	BookUBER vb.Asset = "BOOKUBER" //bookuber
	BMxUBER  vb.Asset = "BMXUBER"  //uber
	USDT     vb.Asset = "USDT"     //Tether
	BMC      vb.Asset = "BMC"      //BMC
	AION     vb.Asset = "AION"     // AION
	APPC     vb.Asset = "APPC"     // APPC
	AST      vb.Asset = "AST"      // AST
	BAT      vb.Asset = "BAT"      // BAT
	BNT      vb.Asset = "BNT"      // BNT
	BRD      vb.Asset = "BRD"      // BRD
	BSV      vb.Asset = "BSV"      // BSV
	CDT      vb.Asset = "CDT"      // CDT
	CVC      vb.Asset = "CVC"      // CVC
	DASH     vb.Asset = "DASH"     // DASH
	DENT     vb.Asset = "DENT"     // DENT
	ELF      vb.Asset = "ELF"      // ELF
	FET      vb.Asset = "FET"      // FET
	FUN      vb.Asset = "FUN"      // FUN
	GNT      vb.Asset = "GNT"      // GNT
	KNC      vb.Asset = "KNC"      // KNC
	MTL      vb.Asset = "MTL"      // MTL
	NPXS     vb.Asset = "NPXS"     // NPXS
	OMG      vb.Asset = "OMG"      // OMG
	POLY     vb.Asset = "POLY"     // POLY
	POWR     vb.Asset = "POWR"     // POWR
	PPT      vb.Asset = "PPT"      // PPT
	QSP      vb.Asset = "QSP"      // QSP
	RDN      vb.Asset = "RDN"      // RDN
	REP      vb.Asset = "REP"      // REP
	SNT      vb.Asset = "SNT"      // SNT
	STORJ    vb.Asset = "STORJ"    // STORJ
	STORM    vb.Asset = "STORM"    // STORM
	TNT      vb.Asset = "TNT"      // TNT
	TUSD     vb.Asset = "TUSD"     // TUSD
	WTC      vb.Asset = "WTC"      // WTC
	XLM      vb.Asset = "XLM"      // XLM
	ZEC      vb.Asset = "ZEC"      // ZEC
	ZIL      vb.Asset = "ZIL"      // ZIL
	ZRX      vb.Asset = "ZRX"      // ZRX
)

// IsBMxToken ...
func IsBMxToken(asset vb.Asset) bool {
	return strings.HasPrefix(string(asset), "BMX")
}

// IsBookToken ...
func IsBookToken(asset vb.Asset) bool {
	return strings.HasPrefix(string(asset), "BOOK")
}

// IsCryptoAsset ...
func IsCryptoAsset(asset vb.Asset) bool {
	return !IsFiat(asset)
}

// IsFiat ...
func IsFiat(currency vb.Asset) bool {
	return currency == EUR || currency == USD || currency == GBP
}

// Truncate ...
func Truncate(asset vb.Asset, v decimal.Decimal) decimal.Decimal {
	v = v.Truncate(6)

	if IsFiat(asset) {
		v = v.Truncate(2)
	}

	return v
}

// Denormalize ...
func Denormalize(amount decimal.Decimal) decimal.Decimal {
	return denormalize(amount, SpotwareValueDecimals)
}

func denormalize(amount decimal.Decimal, decimals int64) decimal.Decimal {
	return amount.DivRound(decimal.New(decimals, 0), 100)
}

// Normalize ...
func Normalize(amount decimal.Decimal) decimal.Decimal {
	return amount.Mul(decimal.New(SpotwareValueDecimals, 0))
}
