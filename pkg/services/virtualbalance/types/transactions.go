package types

import "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"

const (
	TransactionFraudControl         virtualbalance.TransactionType = "fraudControl"
	TransactionPause                virtualbalance.TransactionType = "pause"
	TransactionDepositOnSMW         virtualbalance.TransactionType = "depositOnSMW"
	TransactionSendToTreasury       virtualbalance.TransactionType = "sendToTreasury"
	TransactionEmailNotification    virtualbalance.TransactionType = "emailNotification"
	TransactionSocketNotification   virtualbalance.TransactionType = "socketNotification"
	TransactionBuyOnSMW             virtualbalance.TransactionType = "buyOnSMW"
	TransactionSellOnSMW            virtualbalance.TransactionType = "sellOnSMW"
	TransactionWithdrawOnSMW        virtualbalance.TransactionType = "withdrawOnSMW"
	TransactionSendToUser           virtualbalance.TransactionType = "sendToUser"
	TransactionMixpanelNotification virtualbalance.TransactionType = "mixpanelNotification"
	TransactionEcpForm              virtualbalance.TransactionType = "ecpForm"
	TransactionRegisterBMxHolder    virtualbalance.TransactionType = "registerBMxHolder"
	TransactionSyncBalance          virtualbalance.TransactionType = "syncBalance"
)
