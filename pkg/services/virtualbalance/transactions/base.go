package transactions

import (
	"encoding/json"
	"errors"

	vb "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/decision"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/status"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type base struct {
	log             *zap.Logger
	repo            Repo
	transactionType vb.TransactionType
}

func (t *base) GetType() vb.TransactionType {
	return t.transactionType
}

func (t *base) GetStatus(operationID int64) (vb.Status, error) {
	return t.repo.GetStatus(operationID, t.transactionType)
}

func (t *base) Init(sqlTx *sqlx.Tx, operationID int64, userID int64, value vb.AssetAmount, operationType vb.OperationType, params interface{}) error {
	return t.repo.Init(sqlTx, operationID, t.GetType(), userID, value, operationType, params)
}

func (t *base) emptyPrepare(operationID int64) vb.Decision {
	txStatus, err := t.GetStatus(operationID)
	if err != nil {
		t.log.Error("t.GetStatus", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()), zap.Error(err))

		return decision.Repeat
	}

	if txStatus != status.Initialized {
		t.log.Info("Skip preparing", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()))

		return decision.Continue
	}

	err = t.setStatus(nil, operationID, status.Prepared)
	if err != nil {
		t.log.Error("t.setStatus", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()), zap.Error(err))

		return decision.Repeat
	}

	return decision.Continue
}

func (t *base) emptyRollback(operationID int64) vb.Decision {
	txStatus, err := t.GetStatus(operationID)
	if err != nil {
		t.log.Error("t.GetStatus", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()), zap.Error(err))

		return decision.Repeat
	}

	if txStatus != status.Initialized && txStatus != status.Preparing && txStatus != status.PausedPreparing && txStatus != status.Prepared && txStatus != status.Rollbacking {
		t.log.Info("Skip rollbacking", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()))

		return decision.Continue
	}

	err = t.setStatus(nil, operationID, status.Rollbacked)
	if err != nil {
		t.log.Error("t.setStatus", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()), zap.Error(err))

		return decision.Repeat
	}

	return decision.Continue
}

func (t *base) emptyCommit(operationID int64) vb.Decision {
	txStatus, err := t.GetStatus(operationID)
	if err != nil {
		t.log.Error("t.GetStatus", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()), zap.Error(err))

		return decision.Repeat
	}

	if txStatus == status.Commited {
		t.log.Info("Skip commiting", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()))

		return decision.Continue
	}

	if txStatus != status.Prepared {
		t.log.Error(
			"Skip commiting transaction, wrong status",
			zap.Int64("operationID", operationID),
			zap.Any("type", t.GetType()),
			zap.Any("status", txStatus),
		)

		return decision.Cancel
	}

	err = t.setStatus(nil, operationID, status.Commited)
	if err != nil {
		t.log.Error("t.setStatus", zap.Int64("operationID", operationID), zap.Any("type", t.GetType()), zap.Error(err))

		return decision.Repeat
	}

	return decision.Continue
}

func (t *base) setStatus(tx sqlx.Ext, operationID int64, status vb.Status) error {
	return t.repo.SetStatus(tx, operationID, t.GetType(), status)
}

func (t *base) updateParams(tx sqlx.Ext, operationID int64, transactionType vb.TransactionType, params interface{}) error {
	data, err := json.Marshal(params)
	if err != nil {
		return err
	}

	return t.repo.UpdateParams(tx, operationID, transactionType, data)
}

func newBase(repo Repo, transactionType vb.TransactionType, logger *zap.Logger) (*base, error) {
	if repo == nil {
		return nil, errors.New("empty Repo")
	}

	if logger == nil {
		return nil, errors.New("empty logger")
	}

	return &base{
		log:             logger,
		repo:            repo,
		transactionType: transactionType,
	}, nil
}
