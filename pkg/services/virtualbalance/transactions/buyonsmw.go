package transactions

import (
	"encoding/json"
	"errors"

	"github.com/bfg-dev/crypto-core/pkg/helpers/currency"
	"github.com/bfg-dev/crypto-core/pkg/helpers/db"
	"github.com/bfg-dev/crypto-core/pkg/services/broadcastqueue"
	"github.com/bfg-dev/crypto-core/pkg/services/spotware"
	vb "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/adapters"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/buf"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/decision"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/entities"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/exchangeresult"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/status"
	vbtypes "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/types"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/shopspring/decimal"
	"go.uber.org/zap"
)

type buyOnSMW struct {
	base
	repo                     Repo
	spotwareClient           spotware.ApiClient
	exchangeResultRepo       exchangeresult.Repo
	spotwareUserSettingsRepo spotware.UserOptionsRepo
	balanceService           balance.Service
	spotwareService          spotware.ApiService
	socketEventsProducer     broadcastqueue.Producer
}

type buyOnSMWParams struct {
	SellCurrency vb.Asset        `json:"sellCurrency"`
	Paid         decimal.Decimal `json:"paid"`
}

type buyResultNotification struct {
	socketNotificationParams
	ErrorCode *string  `json:"errorCode"`
	Asset     vb.Asset `json:"asset"`
}

func (t *buyOnSMW) Prepare(operationID int64) vb.Decision {
	log := t.log.With(zap.Int64("operationID", operationID), zap.Any("type", t.GetType()))
	defer log.Sync()

	item, err := t.repo.Get(operationID, t.GetType())
	if err != nil {
		log.Error("repo.Get", zap.Error(err))

		return decision.Repeat
	}

	txStatus := item.Status

	if txStatus != status.Initialized && txStatus != status.Preparing && txStatus != status.PausedPreparing {
		log.Info("Skip preparing")

		return decision.Continue
	}

	assetsMap, err := t.spotwareService.AssetsAsMap()
	if err != nil {
		log.Error("spotwareService.AssetsAsMap", zap.Error(err))

		return decision.Repeat
	}

	symbolsMap, err := t.spotwareService.SymbolsAsMap()
	if err != nil {
		log.Error("spotwareService.SymbolsAsMap", zap.Error(err))

		return decision.Repeat
	}

	params, err := t.getParams(*item)
	if err != nil {
		log.Error("getParams", zap.Error(err))

		return decision.Repeat
	}

	if txStatus == status.PausedPreparing {
		exchangeResult, err := t.exchangeResultRepo.GetByOperationID(operationID)
		if err != nil {
			log.Error("exchangeResultRepo.GetByOperationID", zap.Error(err))

			return decision.Repeat
		}

		if exchangeResult == nil {
			log.Info("exchangeResult is null, pause", zap.Error(err))

			return decision.Pause
		}

		if exchangeResult.ErrorCode != nil {
			log.Info("exchangeResult.ErrorCode not empty, transaction should be rollbacked")

			return decision.Cancel
		}

		_, secondAssetID, err := adapters.GetSymbolAssetsByID(symbolsMap, *exchangeResult.SymbolID)
		if err != nil {
			log.Error("adapters.GetSymbolAssetsByID", zap.Int64("SymbolID", *exchangeResult.SymbolID), zap.Error(err))

			return decision.Cancel
		}

		secondAsset, err := adapters.GetAssetByID(assetsMap, *secondAssetID)
		if err != nil {
			log.Error("adapters.GetAssetByID", zap.Int64("secondAssetID", *secondAssetID), zap.Error(err))

			return decision.Cancel
		}

		sqlTx, err := t.repo.Begin()
		if err != nil {
			log.Error("repo.Begin", zap.Error(err))

			return decision.Repeat
		}

		params.Paid = *exchangeResult.QuoteCurrencyDelta

		if err := t.updateParams(sqlTx, operationID, t.GetType(), params); err != nil {
			log.Error("updateParams", zap.Error(err))
			db.MustRollback(sqlTx)

			return decision.Repeat
		}

		if err := t.balanceService.Add(sqlTx, item.UserID, operationID, vb.Asset(secondAsset.Name), params.Paid); err != nil {
			log.Error("balanceService.Add", zap.Error(err))
			db.MustRollback(sqlTx)

			return decision.Repeat
		}

		if err := t.setStatus(sqlTx, operationID, status.Prepared); err != nil {
			log.Error("setStatus", zap.Error(err))
			db.MustRollback(sqlTx)

			return decision.Repeat
		}

		db.MustCommit(sqlTx)

		if err := t.notifyUser(*item, nil); err != nil {
			log.Error("t.notifyUser, can not notify user about success. User take infinity spinner", zap.Error(err))
			// do not repeat, because notify is not critical
		}

		/*
			@todo
			зеркальная запись
		*/

		return decision.Continue
	}

	if err := t.setStatus(nil, operationID, status.Preparing); err != nil {
		log.Error("setStatus", zap.Error(err))

		return decision.Repeat
	}

	settings, err := t.spotwareUserSettingsRepo.Get(item.UserID)
	if err != nil {
		log.Error("spotwareUserSettingsRepo.Get", zap.Error(err))

		return decision.Repeat
	}

	buyID, err := adapters.GetAssetID(assetsMap, item.Currency)
	if err != nil {
		log.Error("spotwareAdapters.GetAssetID buyID", zap.String("item.Currency", string(item.Currency)), zap.Reflect("assetsMap", assetsMap), zap.Error(err))

		return decision.Cancel
	}

	sellID, err := adapters.GetAssetID(assetsMap, params.SellCurrency)
	if err != nil {
		log.Error("spotwareAdapters.GetAssetID sellID", zap.String("params.SellCurrency", string(params.SellCurrency)), zap.Reflect("assetsMap", assetsMap), zap.Error(err))

		return decision.Cancel
	}

	symbolID, err := adapters.GetSymbolID(symbolsMap, *buyID, *sellID)
	if err != nil {
		log.Error("adapters.GetSymbolID", zap.Any("buyCurrency", *buyID), zap.Any("sellCurrency", *sellID), zap.Error(err))

		return decision.Cancel
	}

	denormalizedVolume := vbtypes.Denormalize(item.Amount)
	amount, fractionalPart := currency.SpotwareVolumeNormalize(denormalizedVolume)
	if fractionalPart != nil {
		// @todo добавить учет этого остатка
	}

	comment := make(types.MapI)
	if err := json.Unmarshal(item.Params, &comment); err != nil {
		log.Error("depositOnSMW unmarshal", zap.Error(err))
	}

	comment["operationID"] = item.OperationID

	data := buf.NewOrderRequest{
		ID:        item.OperationID,
		Volume:    amount,
		SymbolID:  *symbolID,
		TraderID:  settings.TraderID,
		TradeSide: buf.TradeSideBuy,
		Comment:   comment,
	}

	sqlTx, err := t.repo.Begin()
	if err != nil {
		log.Error("repo.Begin", zap.Error(err))

		return decision.Repeat
	}

	if err := t.balanceService.Add(sqlTx, item.UserID, operationID, item.Currency, item.Amount); err != nil {
		log.Error("balanceService.Add", zap.Error(err))
		db.MustRollback(sqlTx)

		return decision.Repeat
	}

	if err := t.setStatus(sqlTx, operationID, status.PausedPreparing); err != nil {
		log.Error("setStatus", zap.Error(err))
		db.MustRollback(sqlTx)

		return decision.Repeat
	}

	db.MustCommit(sqlTx)

	if err := t.spotwareClient.PublishExchangeMessage(data); err != nil {
		log.Error("Need manual processing! spotwareClient.PublishExchangeMessage", zap.Any("data", data), zap.Error(err))
	}

	return decision.Pause
}

func (t *buyOnSMW) Rollback(operationID int64) vb.Decision {
	log := t.log.With(zap.Int64("operationID", operationID), zap.Any("type", t.GetType()))
	defer log.Sync()

	go func() {
		item, err := t.repo.Get(operationID, t.GetType())
		if err != nil {
			log.Error("repo.Get can not get operation for user notification", zap.Error(err))

			return
		}

		exchangeResult, err := t.exchangeResultRepo.GetByOperationID(operationID)
		if err != nil {
			log.Error("exchangeResultRepo.GetByOperationID can not get result for user notification", zap.Error(err))

			return
		}

		err = t.notifyUser(*item, exchangeResult.ErrorCode)
		if err != nil {
			log.Error("t.notifyUser can not notify user", zap.Error(err))
		}
	}()

	return t.emptyRollback(operationID)
}

func (t *buyOnSMW) Commit(operationID int64) vb.Decision {
	return t.emptyCommit(operationID)
}

func (t *buyOnSMW) buildBuyResultNotification(item entities.Transaction, errorCode *string) buyResultNotification {
	return buyResultNotification{
		socketNotificationParams{
			Type:   "Buy result",
			UserID: item.UserID,
		},
		errorCode,
		item.Currency,
	}
}

func (t *buyOnSMW) notifyUser(item entities.Transaction, errorCode *string) error {
	message := t.buildBuyResultNotification(item, errorCode)

	t.log.Info("Sending socket message buy result", zap.Any("message", message))

	jsonValue, err := json.Marshal(message)
	if err != nil {
		return err
	}

	return t.socketEventsProducer.Emit(jsonValue)
}

func (t *buyOnSMW) getParams(item entities.Transaction) (buyOnSMWParams, error) {
	var p buyOnSMWParams

	err := json.Unmarshal(item.Params, &p)
	return p, err
}

func newBuyOnSMW(
	repo Repo,
	balanceService balance.Service,
	exchangeResultRepo exchangeresult.Repo,
	spotwareUserSettingsRepo spotware.UserOptionsRepo,
	spotwareClient spotware.ApiClient,
	spotwareService spotware.ApiService,
	socketEventsProducer broadcastqueue.Producer,
	logger *zap.Logger,
) (vb.Transaction, error) {
	// Внимание! При копипасте не забудь поменять тип транзакции!
	base, err := newBase(repo, vbtypes.TransactionBuyOnSMW, logger)
	if err != nil {
		return nil, err
	}

	if spotwareUserSettingsRepo == nil {
		return nil, errors.New("empty spotwareUserSettingsRepo")
	}

	if spotwareClient == nil {
		return nil, errors.New("empty spotwareClient")
	}

	if exchangeResultRepo == nil {
		return nil, errors.New("empty exchangeResultRepo")
	}

	if balanceService == nil {
		return nil, errors.New("empty balanceService")
	}

	if spotwareService == nil {
		return nil, errors.New("empty spotwareService")
	}

	if socketEventsProducer == nil {
		return nil, errors.New("empty socketEventsProducer")
	}

	return &buyOnSMW{
		*base,
		repo,
		spotwareClient,
		exchangeResultRepo,
		spotwareUserSettingsRepo,
		balanceService,
		spotwareService,
		socketEventsProducer,
	}, nil
}
