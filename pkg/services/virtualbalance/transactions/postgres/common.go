package postgres

import (
	"encoding/json"
	"errors"

	vb "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/entities"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/status"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
)

type transactionRepository struct {
	db *sqlx.DB
}

func (r *transactionRepository) Init(sqlTx sqlx.Ext, operationID int64, transactionType vb.TransactionType, userID int64, value vb.AssetAmount, operationType vb.OperationType, params interface{}) error {
	p, err := json.Marshal(params)
	if err != nil {
		return err
	}

	q := `INSERT INTO "operationsTransactions" ("operationId", "type", "userId", "amount", "currency", "operationType", "params", "status") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`
	_, err = sqlTx.Exec(q, operationID, transactionType, userID, value.Value, value.Currency, operationType, p, status.Initialized)

	return err
}

func (r *transactionRepository) Get(operationID int64, transactionType vb.TransactionType) (*entities.Transaction, error) {
	item := entities.Transaction{}

	row := r.db.QueryRowx(`SELECT "id", "createdAt", "updatedAt", "operationId", "amount", "currency", "userId", "type", "operationType", "externalTransactionId", "status", "params" FROM "operationsTransactions" WHERE "operationId" = $1 and type = $2`, operationID, transactionType)
	if err := row.StructScan(&item); err != nil {
		return nil, err
	}

	return &item, nil
}

func (r *transactionRepository) SetStatus(ext sqlx.Ext, operationID int64, transactionType vb.TransactionType, status vb.Status) error {
	d := sqlx.Ext(r.db)
	if ext != nil {
		d = ext
	}

	q := `UPDATE "operationsTransactions" SET "updatedAt" = current_timestamp, "status" = $1 WHERE type = $2 and "operationId" = $3`
	_, err := d.Exec(q, status, transactionType, operationID)

	return err
}

func (r *transactionRepository) GetStatus(operationID int64, transactionType vb.TransactionType) (vb.Status, error) {
	var status vb.Status

	q := `SELECT status FROM "operationsTransactions" WHERE "operationId" = $1 and type = $2`
	row := r.db.QueryRowx(q, operationID, transactionType)
	err := row.Scan(&status)

	return status, err
}

func (r *transactionRepository) UpdateParams(ext sqlx.Ext, operationID int64, transactionType vb.TransactionType, params []byte) error {
	d := sqlx.Ext(r.db)
	if ext != nil {
		d = ext
	}
	q := `UPDATE "operationsTransactions" SET "updatedAt" = current_timestamp, "params" = $1 WHERE type = $2 and "operationId" = $3`
	_, err := d.Exec(q, params, transactionType, operationID)

	return err
}

func (r *transactionRepository) GetBlockchainTransactionAmountByTransactionID(ID string) (*decimal.Decimal, error) {
	var amount decimal.Decimal

	row := r.db.QueryRowx(`SELECT "amount" FROM "blockchainTransactions" WHERE "transactionId" = $1`, ID)
	if err := row.Scan(&amount); err != nil {
		return nil, err
	}

	return &amount, nil
}

func (r *transactionRepository) Begin() (*sqlx.Tx, error) {
	return r.db.Beginx()
}

// NewRepo ...
func NewRepo(db *sqlx.DB) (transactions.Repo, error) {
	if db == nil {
		return nil, errors.New("db cannot be empty")
	}

	return &transactionRepository{
		db: db,
	}, nil
}
