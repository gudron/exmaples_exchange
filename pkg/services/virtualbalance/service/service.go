package service

import (
	"context"
	"encoding/json"
	"errors"

	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operations"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/queue"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/bfg-dev/crypto-core/pkg/types/btxstatus"
	"go.uber.org/zap"
)

type service struct {
	queueClient    queue.VirtualBalanceQueueClient
	operationsRepo operations.Repo
	logger         *zap.Logger
}

func (s *service) ProcessOutgoingBlockchainTransaction(transaction *types.TrackedTransaction) error {
	// если статус unconfirmed - проставляем в операцию / транзакцию хэш транзакции
	if transaction.Status == btxstatus.Unconfirmed {
		if transaction.ExternalID == nil {
			s.logger.Warn("Empty externalID")

			return nil
		}

		operation, err := s.operationsRepo.Get(*transaction.ExternalID)
		if err != nil {
			return err
		}

		var params operations.WithdrawCryptoParams

		err = json.Unmarshal(operation.Params, &params)
		if err != nil {
			return err
		}

		params.SourceTx = transaction.TxID

		data, err := json.Marshal(params)
		if err != nil {
			return err
		}

		return s.operationsRepo.UpdateParams(*transaction.ExternalID, data)
	}

	// если статус confirmed - инициируем продолжение коммита
	if transaction.Status == btxstatus.Confirmed {
		if err := s.queueClient.PublishCommitMessage(context.TODO(), *transaction.ExternalID); err != nil {
			return err
		}
	}

	return nil
}

// New ...
func New(
	operationsRepo operations.Repo,
	queueClient queue.VirtualBalanceQueueClient,
	logger *zap.Logger,
) (virtualbalance.Service, error) {
	if queueClient == nil {
		return nil, errors.New("vbservice, queueClient cannot be empty")
	}

	if operationsRepo == nil {
		return nil, errors.New("vbservice, operationsRepo cannot be empty")
	}

	if logger == nil {
		return nil, errors.New("vbservice, logger cannot be empty")
	}

	return &service{
		queueClient:    queueClient,
		operationsRepo: operationsRepo,
		logger:         logger,
	}, nil
}
