package virtualbalance

import (
	"strings"

	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
)

// Transaction ...
type Transaction interface {
	Init(sqlTx *sqlx.Tx, operationID int64, userID int64, value AssetAmount, operationType OperationType, params interface{}) error
	Prepare(operationID int64) Decision
	Rollback(operationID int64) Decision
	Commit(operationID int64) Decision
	GetType() TransactionType
	GetStatus(operationID int64) (Status, error)
}

// Operation ...
type Operation interface {
	Init(userID int64, value AssetAmount, externalTransactionID int64, params interface{}) (*int64, error)
	GetIDByExternalID(externalTransactionID int64) (*int64, error)
	Prepare(ID int64) Decision
	Rollback(ID int64) Decision
	Commit(ID int64) Decision
	GetStatus(sqlTx *sqlx.Tx, ID int64) (Status, error)
	GetType() OperationType
}

// Service ...
type Service interface {
	ProcessOutgoingBlockchainTransaction(transaction *types.TrackedTransaction) error
}

// Decision ...
type Decision uint8

// Status ...
type Status string

// TransactionType ...
type TransactionType string

// OperationType ...
type OperationType string

// Asset ...
type Asset string

func (a Asset) String() string {
	return string(a)
}

// GetSymbol ...
func (a Asset) GetSymbol() string {
	if strings.HasPrefix(a.String(), "BMX") {
		return a.String()[3:]
	}

	return a.String()
}

// PauseStatus ...
type PauseStatus string

// BalanceStatus ...
type BalanceStatus string

// AssetAmount ...
type AssetAmount struct {
	Value    decimal.Decimal `json:"operationID"`
	Currency Asset           `json:"currency"`
}
