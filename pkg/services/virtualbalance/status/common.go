package status

import "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"

const (
	Initialized     virtualbalance.Status = "initialized"
	Preparing       virtualbalance.Status = "preparing"
	PausedPreparing virtualbalance.Status = "pausedpreparing"
	Prepared        virtualbalance.Status = "prepared"
	Rollbacking     virtualbalance.Status = "rollbacking"
	Rollbacked      virtualbalance.Status = "rollbacked"
	Commiting       virtualbalance.Status = "commiting"
	Commited        virtualbalance.Status = "commited"
)
