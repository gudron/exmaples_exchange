package amqp

import (
	"fmt"
	"net"
	"time"

	"github.com/bfg-dev/crypto-core/pkg/helpers/platform"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

type connectionProvider struct {
	amqpURL string
	logger  *zap.Logger
}

func (p *connectionProvider) GetConnection() (*amqp.Connection, error) {
	connection, err := amqp.DialConfig(p.amqpURL, amqp.Config{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.DialTimeout(network, addr, 10*time.Second)
		},
	})

	if err != nil {
		return nil, errors.Wrap(err, "ConnectionProvider.GetConnection, unable to init amqp connection")
	}

	return connection, nil
}

func (p *connectionProvider) ProvideConnectionToCb(cb func(connection *amqp.Connection) error, cbName string, maxRetriesCount int64) error {
	var i int64 = 1

	if maxRetriesCount == 0 {
		conn, err := p.GetConnection()

		if err != nil {
			return errors.Wrap(err, "ConnectionProvider.ProvideConnectionToCb, unable to get connection")
		}

		return cb(conn)
	}

	var resultError error
	var connection *amqp.Connection

	for ; i <= maxRetriesCount; i++ {
		p.logger.Info(fmt.Sprintf("ConnectionProvider.ProvideConnectionToCb, try to get channel with callback, attempt #%v", i),
			zap.String("cbName", cbName))
		connection, resultError = p.GetConnection()

		if resultError != nil {
			p.logger.Error("ConnectionProvider.ProvideConnectionToCb, unable to get channel", zap.Error(resultError), zap.String("cbName", cbName))
		} else {
			resultError = cb(connection)

			if resultError != nil {
				connection.Close()
				p.logger.Error("ConnectionProvider.ProvideConnectionToCb, unable exec callback", zap.Error(resultError), zap.String("cbName", cbName))
			} else {
				break
			}
		}

		sleepDuration := time.Second * time.Duration(i)
		p.logger.Info(fmt.Sprintf("ConnectionProvider.ProvideConnectionToCb, sleeping %v seconds", sleepDuration), zap.String("cbName", cbName))
		time.Sleep(sleepDuration)
	}

	if resultError != nil {
		panic(errors.Wrap(resultError, fmt.Sprintf("ConnectionProvider.ProvideConnectionToCb, unable to connect to amqp with %v attempts, cbName: %v", i-1, cbName)))
	}

	return nil
}

func NewConnectionProvider(host string, port int, user string, password string, vhost string, logger *zap.Logger) (ConnectionProvider, error) {
	if len(host) == 0 {
		return nil, nil
	}

	if logger == nil {
		return nil, errors.New("amqp.connectionProvider.NewConnectionProvider, logger must be not empty")
	}

	amqpURL, err := platform.BuildAMQPConnectionUrl(host,
		port,
		user,
		password,
		vhost)

	if err != nil {
		return nil, errors.Wrap(err, "amqp.connectionProvider.NewConnectionProvider, unable to build AMQP connection url")
	}

	return &connectionProvider{
		amqpURL: amqpURL,
		logger:  logger,
	}, nil
}
