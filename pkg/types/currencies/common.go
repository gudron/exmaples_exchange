package currencies

import (
	"errors"
	"fmt"
	"strings"

	"github.com/bfg-dev/crypto-core/pkg/types"
)

const (
	BMC types.Currency = "bmc"
	ETH types.Currency = "eth"
	BTC types.Currency = "btc"
	LTC types.Currency = "ltc"
	USD types.Currency = "usd"
	EUR types.Currency = "eur"
	HKD types.Currency = "hkd"

	BMxiETH  types.Currency = "bmxieth"
	BMxTMR   types.Currency = "bmxtmr"
	BMxVTP   types.Currency = "bmxvtp"
	BMx1DV   types.Currency = "bmx1dv"
	BMxDDR   types.Currency = "bmxddr"
	BMxBN20  types.Currency = "bmxbn20"
	BMxT10   types.Currency = "bmxt10"
	BMxT20   types.Currency = "bmxt20"
	BMxSPY   types.Currency = "bmxspy"
	BMxPMR   types.Currency = "bmxpmr"
	BMxEEM   types.Currency = "bmxeem"
	BMxXMI   types.Currency = "bmxxmi"
	BMxWTR   types.Currency = "bmxwtr"
	BookLYFT types.Currency = "booklyft"
	BMxEGM   types.Currency = "bmxegm"
	BMxBRXT  types.Currency = "bmxbrxt"
	BMxLYFT  types.Currency = "bmxlyft"
	BMxDT10  types.Currency = "bmxdt10"
	BookUBER types.Currency = "bookuber"
	BMxUBER  types.Currency = "bmxuber"

	AION  types.Currency = "aion"
	APPC  types.Currency = "appc"
	AST   types.Currency = "ast"
	BAT   types.Currency = "bat"
	BNT   types.Currency = "bnt"
	BRD   types.Currency = "brd"
	BSV   types.Currency = "bsv"
	CDT   types.Currency = "cdt"
	CVC   types.Currency = "cvc"
	DASH  types.Currency = "dash"
	DENT  types.Currency = "dent"
	ELF   types.Currency = "elf"
	FET   types.Currency = "fet"
	FUN   types.Currency = "fun"
	GNT   types.Currency = "gnt"
	KNC   types.Currency = "knc"
	MTL   types.Currency = "mtl"
	NPXS  types.Currency = "npxs"
	OMG   types.Currency = "omg"
	POLY  types.Currency = "poly"
	POWR  types.Currency = "powr"
	PPT   types.Currency = "ppt"
	QSP   types.Currency = "qsp"
	RDN   types.Currency = "rdn"
	REP   types.Currency = "rep"
	SNT   types.Currency = "snt"
	STORJ types.Currency = "storj"
	STORM types.Currency = "storm"
	TNT   types.Currency = "tnt"
	TUSD  types.Currency = "tusd"
	WTC   types.Currency = "wtc"
	XLM   types.Currency = "xlm"
	ZEC   types.Currency = "zec"
	ZIL   types.Currency = "zil"
	ZRX   types.Currency = "zrx"
)

var (
	currencies        map[types.Currency]bool
	cryptoCurrencies  map[types.Currency]bool
	fiatCurrencies    map[types.Currency]bool
	displayCurrencies map[types.Currency]bool
	bmxCurrencies     types.CurrencyList
)

// IsCurrency ...
func IsCurrency(currency types.Currency) bool {
	_, ok := currencies[currency]
	return ok
}

// IsCryptoCurrency ...
func IsCryptoCurrency(currency types.Currency) bool {
	_, ok := cryptoCurrencies[currency]
	return ok
}

// IsBMxAsset ...
func IsBMxAsset(currency types.Currency) bool {
	return strings.HasPrefix(string(currency), "bmx")
}

// IsFiatCurrency ...
func IsFiatCurrency(currency types.Currency) bool {
	_, ok := fiatCurrencies[currency]
	return ok
}

// IsDisplayCurrency ...
func IsDisplayCurrency(currency types.Currency) bool {
	_, ok := displayCurrencies[currency]
	return ok
}

// GetBmxCurrencies ...
func GetBmxCurrencies() (types.CurrencyList, error) {
	if !bmxCurrencies.IsEmpty() {
		return bmxCurrencies, nil
	}
	return bmxCurrencies, errors.New("GetBmxCurrencies.CurrencyList is empty")
}

// GetByAssetSymbol ...
func GetByAssetSymbol(s string) *types.Currency {
	s = fmt.Sprintf("bmx%s", s)

	var a types.Currency

	switch s {
	case string(BMxiETH):
		a = BMxiETH
	case string(BMxTMR):
		a = BMxTMR
	case string(BMxVTP):
		a = BMxVTP
	case string(BMx1DV):
		a = BMx1DV
	case string(BMxDDR):
		a = BMxDDR
	case string(BMxBN20):
		a = BMxBN20
	case string(BMxT10):
		a = BMxT10
	case string(BMxT20):
		a = BMxT20
	case string(BMxSPY):
		a = BMxSPY
	case string(BMxPMR):
		a = BMxPMR
	case string(BMxEEM):
		a = BMxEEM
	case string(BMxXMI):
		a = BMxXMI
	case string(BMxWTR):
		a = BMxWTR
	case string(BookLYFT):
		a = BookLYFT
	case string(BMxEGM):
		a = BMxEGM
	case string(BMxBRXT):
		a = BMxBRXT
	case string(BMxLYFT):
		a = BMxLYFT
	case string(BMxDT10):
		a = BMxDT10
	case string(BookUBER):
		a = BookUBER
	case string(BMxUBER):
		a = BMxUBER
	default:
		return nil
	}

	return &a
}

func init() {
	cryptoCurrencies = map[types.Currency]bool{
		BMC: true,
		ETH: true,
		BTC: true,
		LTC: true,
	}

	fiatCurrencies = map[types.Currency]bool{
		USD: true,
		EUR: true,
		HKD: true,
	}

	displayCurrencies = map[types.Currency]bool{
		USD: true,
		EUR: true,
		ETH: true,
		BTC: true,
	}

	currencies = map[types.Currency]bool{
		BMC: true,
		ETH: true,
		BTC: true,
		LTC: true,
		USD: true,
		EUR: true,
		HKD: true,
	}

	bmxCurrencies = types.CurrencyList{
		BMxiETH, BMxTMR, BMxVTP, BMx1DV, BMxDDR, BMxBN20, BMxT10,
		BMxT20, BMxSPY, BMxPMR, BMxEEM, BMxXMI, BMxWTR, BMxEGM,
		BMxBRXT, BMxLYFT, BMxDT10, BookUBER, BMxUBER,
	}
}
