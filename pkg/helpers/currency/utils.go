package currency

import (
	"math"
	"strconv"
	"strings"

	vbtypes "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/types"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/bfg-dev/crypto-core/pkg/types/currencies"
	"github.com/shopspring/decimal"
)

const (
	bmcDecimals  = 100000000
	atxDecimals  = 10000000000
	ethDecimals  = 1000000000000000000
	fiatDecimals = 100
)

func normalize(amount float64, decimals int64) decimal.Decimal {
	return decimal.NewFromFloat(amount).Mul(decimal.New(decimals, 0))
}

func denormalize(amount decimal.Decimal, decimals int64) decimal.Decimal {
	return amount.DivRound(decimal.New(decimals, 0), 100)
}

func NormalizeBmc(amount float64) decimal.Decimal {
	return normalize(amount, bmcDecimals)
}

func NormalizeATx(amount decimal.Decimal) decimal.Decimal {
	return amount.Mul(decimal.New(atxDecimals, 0))
}

func NormalizeEth(amount float64) decimal.Decimal {
	return normalize(amount, ethDecimals)
}

func NormalizeFiat(amount float64) decimal.Decimal {
	return normalize(amount, fiatDecimals)
}

func DenormalizeFiat(amount decimal.Decimal) decimal.Decimal {
	return denormalize(amount, fiatDecimals)
}

func DenormalizeEth(amount decimal.Decimal) decimal.Decimal {
	return denormalize(amount, ethDecimals)
}

func DenormalizeBmc(amount decimal.Decimal) decimal.Decimal {
	return denormalize(amount, bmcDecimals)
}

func DenormalizeATx(amount decimal.Decimal) decimal.Decimal {
	return denormalize(amount, atxDecimals)
}

func Denormalize(amount decimal.Decimal, c types.Currency) decimal.Decimal {
	switch true {
	case c == currencies.ETH:
		return denormalize(amount, ethDecimals)
	case c == currencies.BTC || c == currencies.LTC || c == currencies.BMC:
		return denormalize(amount, bmcDecimals)
	case currencies.IsFiatCurrency(c):
		return denormalize(amount, fiatDecimals)
	}

	return denormalize(amount, atxDecimals)
}

// SpotwareValueNormalize ...
func SpotwareValueNormalize(d decimal.Decimal) (uint64, *string) {
	d = d.Mul(decimal.New(vbtypes.SpotwareValueDecimals, 0))
	s := strings.Split(d.String(), ".")
	intPart, _ := strconv.ParseUint(s[0], 10, 64)

	if len(s) == 1 {
		return intPart, nil
	}

	return intPart, &s[1]
}

// SpotwareVolumeNormalize ...
func SpotwareVolumeNormalize(d decimal.Decimal) (uint64, *string) {
	d = d.Mul(decimal.New(vbtypes.SpotwareVolumeDecimals, 0))
	s := strings.Split(d.String(), ".")
	intPart, _ := strconv.ParseUint(s[0], 10, 64)

	if len(s) == 1 {
		return intPart, nil
	}

	return intPart, &s[1]
}

func Normalize(amount decimal.Decimal, c types.Currency) decimal.Decimal {
	switch true {
	case c == currencies.BTC || c == currencies.LTC || c == currencies.BMC:
		return amount.Mul(decimal.New(bmcDecimals, 0))
	case c == currencies.ETH:
		return amount.Mul(decimal.New(ethDecimals, 0))
	case currencies.IsFiatCurrency(c):
		return amount.Mul(decimal.New(fiatDecimals, 0))
	}

	return amount.Mul(decimal.New(atxDecimals, 0))
}

func RoundNearestDigit(amount decimal.Decimal) decimal.Decimal {
	log, _ := amount.Float64()
	log = math.Trunc(math.Log10(log))

	if log > 0 {
		return amount.Truncate(0)
	}

	return amount.Truncate(1 - int32(log))
}
