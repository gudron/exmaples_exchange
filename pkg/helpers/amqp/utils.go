package amqp

import (
	"encoding/json"
	"fmt"

	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

// buildJSONMessage builds message with content type = application/json
func buildJSONMessage(body interface{}, headers amqp.Table) (*amqp.Publishing, error) {
	bodyJSON, err := json.Marshal(body)

	if err != nil {
		return nil, errors.New("ampq.BuildJSONMessage, unable to convert body to JSON")
	}

	msg := amqp.Publishing{
		Headers:      headers,
		ContentType:  "application/json",
		Body:         bodyJSON,
		DeliveryMode: amqp.Persistent,
	}

	return &msg, nil
}

// BuildDeadExchangeName ...
func BuildDeadExchangeName(e types.AmqpExchangeName) string {
	return string(e) + "-dead"
}

//BuildUniqName build name with uniq postfix
func BuildUniqName(name string, uniq string) string {
	if len(uniq) == 0 {
		return name
	}

	return fmt.Sprintf("%v-%v", name, uniq)
}
