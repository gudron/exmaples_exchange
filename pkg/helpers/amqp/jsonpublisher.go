package amqp

import (
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

type jsonPublisher struct {
	body    interface{}
	headers amqp.Table
}

//GetBody return body
func (jp *jsonPublisher) GetBody() interface{} {
	return jp.body
}

//GetHeaders return  HeadersTable
func (jp *jsonPublisher) GetHeaders() amqp.Table {
	return jp.headers
}

//AddBody add headers to HeadersTable
func (jp *jsonPublisher) AddHeaders(key string, val interface{}) {
	jp.headers[key] = val
}

//Encode json message for publishing
func (jp *jsonPublisher) Encode() (*amqp.Publishing, error) {
	return buildJSONMessage(jp.GetBody(), jp.GetHeaders())
}

//TODO: should implement publishing to amqp queue
func (jp *jsonPublisher) Publish() {
	panic("implement me")
}

//NewJsonPublisher return Publisher interface
func NewJsonPublisher(body interface{}) (Publisher, error) {

	if body == nil {
		return nil, errors.New("Jsonpublisher -  body argument can not be empty")
	}

	return &jsonPublisher{
		body:    body,
		headers: amqp.Table{},
	}, nil
}
