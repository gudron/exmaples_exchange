package amqp

import "github.com/streadway/amqp"

// Publisher ...
type Publisher interface {
	GetBody() interface{}
	GetHeaders() amqp.Table
	AddHeaders(key string, val interface{})
	Encode() (*amqp.Publishing, error)
	Publish()
}
