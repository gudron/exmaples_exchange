package helpers

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
)

// GetSHA256String ...
func GetSHA256String(secret, data string) string {
	hash := hmac.New(sha256.New, []byte(secret))
	hash.Write([]byte(data))

	return hex.EncodeToString(hash.Sum(nil))
}
