package api

import (
	"encoding/json"
	v "github.com/go-ozzo/ozzo-validation"
	"io"
	"net/http"
)

type EmptySuccess map[string]string

// Response contains Success flag, Errors  (if occurred, nil otherwise) and Data (nil if no data)
type Response struct {
	Success  bool        `json:"success"`
	Errors   interface{} `json:"errors"`
	Data     interface{} `json:"data"`
	HttpCode int         `json:"-"`
}

//ErrorResponse is helper for fast error response creation
func ErrorResponse(errors interface{}) *Response {
	return &Response{
		Success:  false,
		Errors:   errors,
		Data:     nil,
		HttpCode: http.StatusBadRequest,
	}
}

//ErrorResponseWithData is helper for fast error response creation
func ErrorResponseWithData(errors interface{}, data interface{}) *Response {
	return &Response{
		Success:  false,
		Errors:   errors,
		Data:     data,
		HttpCode: http.StatusBadRequest,
	}
}

func AccessDeniedResponse(errors interface{}) *Response {
	return &Response{
		Success:  false,
		Errors:   errors,
		Data:     nil,
		HttpCode: http.StatusForbidden,
	}
}

//ServerErrorResponse is helper for fast server error response creation
func ServerErrorResponse(serverErrorMessage string, httpCode int) *Response {
	return &Response{
		Success:  false,
		Errors:   map[string]string{"serverError": serverErrorMessage},
		Data:     nil,
		HttpCode: httpCode,
	}
}

//InternalServerError is helper for fast server error response creation
func InternalServerError() *Response {
	return ServerErrorResponse("internal server error", http.StatusInternalServerError)
}

//WrongParamsError is helper for fast params error response creation
func WrongParamsError() *Response {
	return ServerErrorResponse("wrong params", http.StatusBadRequest)
}

//SuccessResponse is helper for fast success response creation
func SuccessResponse(data interface{}) *Response {
	return &Response{
		Success:  true,
		Errors:   map[string]error{},
		Data:     data,
		HttpCode: http.StatusOK,
	}
}

func EmptySuccessResponse() *Response {
	return SuccessResponse(EmptySuccess{})
}

//ToJSON Encode response to json and send to writer
func ToJSON(w io.Writer, response Response) error {
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)

	return enc.Encode(response)
}

func ValidationErrorsByFields(validationErrors v.Errors) v.Errors {
	errorsFiltered := validationErrors.Filter()

	if errorsFiltered != nil {
		errorsByFields := errorsFiltered.(v.Errors)

		if len(errorsByFields) != 0 {
			return errorsByFields
		}
	}

	return nil
}
