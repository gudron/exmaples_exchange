package presenters

import (
	"github.com/bfg-dev/crypto-core/pkg/entities"
	"github.com/bfg-dev/crypto-core/pkg/services/currencyrates"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/shopspring/decimal"
)

//Asset contains part of asset fields
type Asset struct {
	ID                int64  `json:"id"`
	Name              string `json:"name"`
	Symbol            string `json:"symbol"`
	Contract          string `json:"contract"`
	ReturnRisk        int16  `json:"returnRisk"`
	ChangeDescription string `json:"changeDescription"`
}

//NewAsset creates Asset presenter from Asset
func NewAsset(asset *entities.Asset) *Asset {
	result := Asset{}
	if asset == nil {
		return &result
	}

	result.ID = asset.ID
	result.Name = asset.Name
	result.Symbol = asset.Symbol
	result.Contract = asset.Contract
	result.ReturnRisk = asset.ReturnRisk
	result.ChangeDescription = asset.ChangeDescription

	return &result
}

//NewAssetList creates list of assets
func NewAssetList(assets []entities.Asset) []Asset {
	result := make([]Asset, 0, len(assets))
	for _, asset := range assets {
		result = append(result, *NewAsset(&asset))
	}

	return result
}

func NewSymbolAssets(assets []entities.Asset) []string {
	result := make([]string, 0, len(assets))
	for _, asset := range assets {
		result = append(result, asset.Symbol)
	}

	return result
}

// AssetsReturnPriceItem ...
type AssetsReturnPriceItem struct {
	Price  decimal.Decimal `json:"tokenPrice"`
	Return decimal.Decimal `json:"return"`
}

// AssetsPriceReturnMap ...
type AssetsPriceReturnMap map[string]map[types.Currency]AssetsReturnPriceItem

// NewAssetReturns ...
func NewAssetReturns(assets entities.Assets, returnsRecords entities.AssetPriceReturnRecords) AssetsPriceReturnMap {
	result := make(AssetsPriceReturnMap)
	assetsMap := assets.MarshalToByIDMap()

	for _, record := range returnsRecords {
		assetSymbol := assetsMap[record.AssetID].Symbol

		if _, ok := result[assetSymbol]; !ok {
			result[assetSymbol] = make(map[types.Currency]AssetsReturnPriceItem)
		}

		result[assetSymbol][record.Currency] = AssetsReturnPriceItem{
			Price:  record.AssetPrice,
			Return: record.AssetReturn,
		}
	}

	return result
}

// NewAssetReturn ...
func NewAssetReturn(asset entities.Asset, rate currencyrates.ReturnPriceResponseResult) AssetsPriceReturnMap {
	result := make(AssetsPriceReturnMap)

	for currency, currencyData := range rate[asset.Symbol] {
		if _, ok := result[asset.Symbol]; !ok {
			result[asset.Symbol] = make(map[types.Currency]AssetsReturnPriceItem)
		}

		result[asset.Symbol][currency] = AssetsReturnPriceItem{
			Price:  currencyData.Price,
			Return: currencyData.Return,
		}
	}

	return result
}
