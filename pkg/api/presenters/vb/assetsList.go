package vb

import (
	vbService "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/entities"
	vbEntities "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/entities"
	vbtypes "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/types"
	"github.com/shopspring/decimal"
)

// Asset ...
type Asset struct {
	Name        *vbService.Asset  `json:"name"`
	DisplayName string            `json:"displayName"`
	Description string            `json:"description"`
	RoutingKey  *string           `json:"routingKey"`
	Sign        string            `json:"sign"`
	CanDeposit  bool              `json:"canDeposit"`
	CanWithdraw bool              `json:"canWithdraw"`
	Cash        []vbService.Asset `json:"cash"`
	CanBuy      bool              `json:"canBuy"`
	CanSell     bool              `json:"canSell"`
	KycOnly     bool              `json:"kycOnly"`
}

// AssetWithBalance ...
type AssetWithBalance struct {
	Asset
	Balance decimal.Decimal `json:"balance"`
}

func NewAsset(
	asset vbEntities.VbAsset,
	balance vbEntities.AssetBalance,
) (*AssetWithBalance, error) {
	item := AssetWithBalance{
		Asset: Asset{
			Name:        asset.Name,
			DisplayName: asset.DisplayName,
			Description: asset.Description,
			RoutingKey:  asset.RoutingKey,
			Sign:        asset.Sign,
			CanDeposit:  asset.CanDeposit,
			CanWithdraw: asset.CanWithdraw,
			Cash:        asset.Cash,
			CanBuy:      asset.CanBuy,
			CanSell:     asset.CanSell,
			KycOnly:     asset.KycOnly,
		},
		Balance: vbtypes.Denormalize(balance.Balance),
	}

	return &item, nil
}

// NewAssetsList ...
func NewAssetsList(
	assets []vbEntities.VbAsset,
	balances []entities.AssetBalance,
) (*[]AssetWithBalance, error) {

	balancesMap := map[vbService.Asset]decimal.Decimal{}

	for _, balance := range balances {
		balancesMap[balance.Asset] = vbtypes.Denormalize(balance.Balance)
	}

	result := make([]AssetWithBalance, len(assets))

	for i, asset := range assets {

		item := AssetWithBalance{
			Asset: Asset{
				Name:        asset.Name,
				DisplayName: asset.DisplayName,
				Description: asset.Description,
				RoutingKey:  asset.RoutingKey,
				Sign:        asset.Sign,
				CanDeposit:  asset.CanDeposit,
				CanWithdraw: asset.CanWithdraw,
				Cash:        asset.Cash,
				CanBuy:      asset.CanBuy,
				CanSell:     asset.CanSell,
				KycOnly:     asset.KycOnly,
			},
		}

		if asset.Name != nil {
			assetName := *asset.Name

			if balance, ok := balancesMap[assetName]; ok {
				item.Balance = balance
			}
		}

		result[i] = item
	}

	return &result, nil
}
