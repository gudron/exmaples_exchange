package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/bfg-dev/crypto-core/pkg/bfgerrors"
	"github.com/bfg-dev/crypto-core/pkg/entities"
	"github.com/bfg-dev/crypto-core/pkg/services"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type HandlerWithReturn func(w http.ResponseWriter, req *http.Request) (*Response, error)

func ResponseHandler(withReturn HandlerWithReturn) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		resp, err := withReturn(w, req)

		if err != nil {
			fields := make([]zapcore.Field, 0, 0)

			user, ok := req.Context().Value("user").(*entities.User)

			if ok == true && user != nil {
				fields = append(fields, zap.Int64("userId", user.ID))
			}

			message := "internal error was occurred"

			switch err.(type) {
			case *bfgerrors.ApiInternalError:
				apiInternalError := err.(*bfgerrors.ApiInternalError)
				message = fmt.Sprintf("%v, %v", apiInternalError.MethodName, apiInternalError.Message)

				if apiInternalError.MethodParams != nil {
					jsonMethodParams, err := json.Marshal(apiInternalError.MethodParams)

					if err != nil {
						fields = append(fields, zap.String("params", string(jsonMethodParams)))
					} else {
						fields = append(fields, zap.Any("params", apiInternalError.MethodParams))
					}
				}

				if apiInternalError.Context != nil {
					for key, val := range apiInternalError.Context {
						fields = append(fields, zap.Any(key, val))
					}
				}

				if apiInternalError.Err != nil {
					fields = append(fields, zap.Error(apiInternalError.Err))
				}

			default:
				fields = append(fields, zap.Error(err))
			}

			services.Application().Logger().Error(message, fields...)
			resp = InternalServerError()
		}

		if resp != nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(resp.HttpCode)
			ToJSON(w, *resp)
		}
	})
}
