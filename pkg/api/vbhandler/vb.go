package vbhandler

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/bfg-dev/crypto-core/pkg/api"
	"github.com/bfg-dev/crypto-core/pkg/api/params"
	vbParams "github.com/bfg-dev/crypto-core/pkg/api/params/vb"
	"github.com/bfg-dev/crypto-core/pkg/api/presenters"
	vbPresenters "github.com/bfg-dev/crypto-core/pkg/api/presenters/vb"
	"github.com/bfg-dev/crypto-core/pkg/bfgerrors"
	"github.com/bfg-dev/crypto-core/pkg/entities"
	"github.com/bfg-dev/crypto-core/pkg/helpers/db"
	spHelper "github.com/bfg-dev/crypto-core/pkg/helpers/spotware"
	"github.com/bfg-dev/crypto-core/pkg/services"
	legacyAssetService "github.com/bfg-dev/crypto-core/pkg/services/asset"
	"github.com/bfg-dev/crypto-core/pkg/services/blockchain/addresscontrol"
	"github.com/bfg-dev/crypto-core/pkg/services/blockchain/assetcontract"
	"github.com/bfg-dev/crypto-core/pkg/services/cards"
	ecpTypes "github.com/bfg-dev/crypto-core/pkg/services/cards/ecp/types"
	cardTypes "github.com/bfg-dev/crypto-core/pkg/services/cards/types"
	"github.com/bfg-dev/crypto-core/pkg/services/currencyrates"
	"github.com/bfg-dev/crypto-core/pkg/services/cyclicaddress/interfaces"
	cyclicAddressInterfaces "github.com/bfg-dev/crypto-core/pkg/services/cyclicaddress/interfaces"
	"github.com/bfg-dev/crypto-core/pkg/services/ethaddress"
	"github.com/bfg-dev/crypto-core/pkg/services/spotware"
	"github.com/bfg-dev/crypto-core/pkg/services/users"
	"github.com/bfg-dev/crypto-core/pkg/services/users/countryperson"
	vb "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/adapters"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/assets"
	vbBalance "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance"
	vbEntities "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/entities"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operations"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/orders"
	pauseRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/pause"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/queue"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/tradingvolumes"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions"
	vbtypes "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/types"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/bfg-dev/crypto-core/pkg/types/addresspurpose"
	"github.com/bfg-dev/crypto-core/pkg/types/contracttype"
	"github.com/bfg-dev/crypto-core/pkg/types/currencies"
	"github.com/bfg-dev/crypto-helpers/pkg/services/blockchain"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"go.uber.org/zap"
)

// VbHandler ...
type VbHandler struct {
	app                          services.App
	currencyRatesService         currencyrates.Service
	operationRepo                operations.Repo
	pauseRepo                    pauseRepository.Repo
	transactionsStrategy         transactions.Strategy
	vbQueueClient                queue.VirtualBalanceQueueClient
	vbAssetsService              assets.Service
	poolRegistry                 cyclicAddressInterfaces.PoolRegistry
	vbBalanceService             vbBalance.Service
	currencyRates                currencyrates.Service
	legacyAssetService           legacyAssetService.Service
	countryPersonDetectorService countryperson.DetectorService
	usersRepository              users.Repository
	addressControlService        addresscontrol.Service
	blockchainReaderService      blockchain.ReaderService
	cardsService                 cards.Service
	cardsAmqpClient              cards.AmqpClient
	baseURL                      string
	spotwareApiService           spotware.ApiService
	ethAddressService            ethaddress.Service
	ordersService                *orders.Service
}

var defaultLimitEUR = 1000.00 // eur

// HandleSell ...
func (h *VbHandler) HandleSell(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	assets, err := h.vbAssetsService.GetEnabledNames()
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetEnabledNames", zap.Error(err))
		return api.InternalServerError(), nil
	}

	methodParams := vbParams.NewSellParams(assets)
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	sellCurrency := vb.Asset(methodParams.SellCurrency)
	buyCurrency := vb.Asset(methodParams.BuyCurrency)

	sellAsset, err := h.vbAssetsService.GetByName(sellCurrency)
	if err != nil || sellAsset.CanSell == false || (sellAsset.KycOnly && user.KYCPassed == false) {
		h.app.Logger().Error("HandleSell, assetsService.GetByNames, sell asset not found or can not be sell ",
			zap.Any("sellCurrency", sellCurrency),
			zap.Any("buyCurrency", buyCurrency),
			zap.Int64("userID", user.ID),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "can not find sell asset"}), nil
	}

	sellBalance, err := h.vbBalanceService.GetBalanceByAsset(user.ID, sellCurrency)
	if err != nil {
		h.app.Logger().Error("HandleSell, vbBalanceService.GetBalanceByAsset, can not get balance by sell asset ",
			zap.Any("sellAsset", sellAsset),
			zap.Int64("user.ID", user.ID),
			zap.Error(err))
		return api.InternalServerError(), nil
	}

	sellBalanceDenormalized := vbtypes.Denormalize(*sellBalance)

	if sellBalanceDenormalized.LessThan(methodParams.SellAmount) {
		return api.ErrorResponse(map[string]string{"sellAmount": "Insufficient funds on balance"}), nil
	}

	buyAsset, err := h.vbAssetsService.GetByName(buyCurrency)

	if err != nil || buyAsset.CanBuy == false || (buyAsset.KycOnly && user.KYCPassed == false) {
		h.app.Logger().Error("HandleSell, assetsService.GetByNames, buy asset not found or can not be buy",
			zap.Any("sellCurrency", sellCurrency),
			zap.Any("buyCurrency", buyCurrency),
			zap.Int64("userID", user.ID),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "can not find buy asset"}), nil
	}

	transactions, err := h.transactionsStrategy.GetList(vbtypes.OperationSell, nil)
	if err != nil {
		h.app.Logger().Error("transactionsStrategy.GetList", zap.Error(err))
		return api.InternalServerError(), nil
	}

	operation := operations.New(h.operationRepo, transactions, h.vbBalanceService, h.app.Logger())
	operationParams := operations.SellParams{
		BuyCurrency: buyCurrency,
	}

	amount := vbtypes.Normalize(methodParams.SellAmount)

	operationID, err := operation.Init(
		user.ID,
		vb.AssetAmount{Value: amount, Currency: sellCurrency},
		0,
		operationParams,
	)
	if err != nil {
		h.app.Logger().Error("operation.Init", zap.Error(err))
		return api.InternalServerError(), nil
	}

	if operationID != nil {
		if err := h.vbQueueClient.PublishPrepareMessage(context.TODO(), *operationID); err != nil {
			h.app.Logger().Error(
				"HandleSell, vbQueueClient.PublishPrepareMessage",
				zap.Int64("operationID", *operationID),
				zap.Error(err),
			)
		}
	}

	return api.SuccessResponse(vbPresenters.NewSell()), nil
}

// HandleBuy ...
func (h *VbHandler) HandleBuy(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	// TODO: use this error pattern for amount errors
	// return api.ErrorResponse(map[string]string{"amount": "Amount invalid."}), nil
	user := api.MustGetUserFromContext(req)

	assets, err := h.vbAssetsService.GetEnabledNames()
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetEnabledNames", zap.Error(err))
		return api.InternalServerError(), nil
	}

	methodParams := vbParams.NewBuyParams(assets)
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	sellCurrency := vb.Asset(methodParams.SellCurrency)
	buyCurrency := vb.Asset(methodParams.BuyCurrency)

	sellAsset, err := h.vbAssetsService.GetByName(sellCurrency)

	if err != nil || sellAsset.CanSell == false || (sellAsset.KycOnly && user.KYCPassed == false) {
		h.app.Logger().Error("HandleBuy, assetsService.GetByNames, sell asset not found or can not be sell ",
			zap.Any("sellCurrency", sellCurrency),
			zap.Any("buyCurrency", buyCurrency),
			zap.Int64("userID", user.ID),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "can not find sell asset"}), nil
	}

	buyAsset, err := h.vbAssetsService.GetByName(buyCurrency)

	if err != nil || buyAsset.CanBuy == false || (buyAsset.KycOnly && user.KYCPassed == false) {
		h.app.Logger().Error("HandleBuy, assetsService.GetByNames, buy asset not found or can not be buy",
			zap.Any("sellCurrency", sellCurrency),
			zap.Any("buyCurrency", buyCurrency),
			zap.Int64("userID", user.ID),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "can not find buy asset"}), nil
	}

	transactions, err := h.transactionsStrategy.GetList(vbtypes.OperationBuy, nil)
	if err != nil {
		h.app.Logger().Error("transactionsStrategy.GetList", zap.Error(err))
		return api.InternalServerError(), nil
	}

	operation := operations.New(h.operationRepo, transactions, h.vbBalanceService, h.app.Logger())
	operationParams := operations.BuyParams{
		SellCurrency: sellCurrency,
	}

	amount := vbtypes.Normalize(methodParams.BuyAmount)

	operationID, err := operation.Init(
		user.ID,
		vb.AssetAmount{Value: amount, Currency: buyCurrency},
		0,
		operationParams,
	)
	if err != nil {
		h.app.Logger().Error("operation.Init", zap.Error(err))
		return api.InternalServerError(), nil
	}

	if operationID != nil {
		if err := h.vbQueueClient.PublishPrepareMessage(req.Context(), *operationID); err != nil {
			h.app.Logger().Error(
				"HandleBuy, vbQueueClient.PublishPrepareMessage",
				zap.Int64("operationID", *operationID),
				zap.Error(err),
			)
		}
	}

	return api.SuccessResponse(vbPresenters.NewBuy()), nil
}

// HandleDeposit ...
func (h *VbHandler) HandleDeposit(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	assets, err := h.vbAssetsService.GetEnabledNames()
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetEnabledNames", zap.Error(err))
		return api.InternalServerError(), nil
	}

	methodParams := vbParams.NewDepositParams(assets)
	params.MustDecodeParams(req.Body, &methodParams)

	h.app.Logger().Info("HandleDeposit", zap.Any("assets", assets), zap.Any("methodParams", methodParams))

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	asset := vb.Asset(strings.ToUpper(methodParams.Currency))
	currency := types.Currency(strings.ToLower(methodParams.Currency))

	uCurrency := strings.ToUpper(methodParams.Currency)
	minLimit, err := h.vbAssetsService.GetMinimumDeposit(vb.Asset(uCurrency))
	if err != nil {
		h.app.Logger().Error("HandleDeposit, GetMinimumDeposit", zap.String("asset", methodParams.Currency), zap.Error(err))

		return nil, bfgerrors.NewApiIntErr(err, methodParams, "GetMinimumDeposit", nil)
	}

	// Если депозитим BMx - выделяем адрес из ETH-пула
	vbAsset := false

	if vbtypes.IsBMxToken(asset) || asset == vbtypes.BMC {
		vbAsset = true
		currency = currencies.ETH
	}

	pool, err := h.poolRegistry.Get(currency)
	if err != nil {
		h.app.Logger().Error("HandleDeposit, poolRegistry.Get", zap.String("currency", methodParams.Currency), zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, methodParams, "unable to get pool by currency", nil)
	}

	details := map[string]interface{}{}

	if vbAsset {
		legacyAsset, err := h.legacyAssetService.GetAssetBySymbol(strings.ToLower(asset.GetSymbol()))
		if err != nil || legacyAsset == nil {
			h.app.Logger().Error("HandleDeposit, legacyAssetService.GetAssetBySymbol", zap.String("currency", strings.ToLower(asset.GetSymbol())), zap.Error(err))

			return nil, bfgerrors.NewApiIntErr(err, methodParams, "GetAssetBySymbol", nil)
		}

		details["assetId"] = legacyAsset.ID
		details["assetSymbol"] = legacyAsset.Symbol

		address, err := pool.GetByDetailsAssetID(user.ID, legacyAsset.ID, addresspurpose.VbDeposit)
		if err != nil {
			h.app.Logger().Error(
				"HandleDeposit, pool.GetByDetailsAssetID",
				zap.Int64("userID", user.ID),
				zap.Int64("legacyAssetID", legacyAsset.ID),
				zap.Error(err),
			)

			return nil, bfgerrors.NewApiIntErr(err, methodParams, "GetRentedAddressesByUserIDAndPurpose", nil)
		}

		if address == nil {
			err := h.depositRentBMxAddress(pool, currency, *legacyAsset, user.ID, details)
			if err != nil {
				h.app.Logger().Error("HandleDeposit, depositRentBMxAddress", zap.Error(err))

				return nil, bfgerrors.NewApiIntErr(err, methodParams, "GetAssetBySymbol", nil)
			}

			return api.SuccessResponse(
				vbPresenters.NewDeposit("", "", nil, false, &minLimit),
			), nil
		}

		alreadyRentedAddress := address.GetAddress()
		registered, err := h.isRegistered(*legacyAsset, h.addressControlService.GetAssetContractRepository(), alreadyRentedAddress)
		if err != nil {
			h.app.Logger().Error("HandleDeposit, h.isRegistered",
				zap.Int64("assetId", legacyAsset.ID),
				zap.Error(err),
			)

			return nil, bfgerrors.NewApiIntErr(err, methodParams, "isRegistered", nil)
		}

		if !registered {
			return api.SuccessResponse(
				vbPresenters.NewDeposit("", "", nil, false, &minLimit),
			), nil
		}

		return api.SuccessResponse(
			vbPresenters.NewDeposit(
				fmt.Sprintf("/qr/qr?content=%s&size=200", alreadyRentedAddress),
				alreadyRentedAddress,
				nil,
				true,
				&minLimit,
			),
		), nil
	}

	now := time.Now()
	rentDuration := time.Hour * interfaces.MaxAddressRentHours
	availableUntil := now.Add(time.Hour * interfaces.MaxAddressRentHours)

	sqlTx := h.app.DBConnection().MustBegin()

	address, err := pool.Rent(user.ID, currency, addresspurpose.VbDeposit, details, rentDuration, sqlTx)
	if err != nil {
		db.MustRollback(sqlTx)
		h.app.Logger().Error("HandleDeposit, pool.Rent", zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, methodParams, "unable to get pool by currency", nil)
	}

	if address == nil {
		h.app.Logger().Error("HandleDeposit, unable to get available address",
			zap.String("currency", methodParams.Currency),
			zap.Error(err))
		db.MustRollback(sqlTx)

		return api.ErrorResponse(map[string]string{"error": "unable to get address from pool, try later"}), nil
	}

	db.MustCommit(sqlTx)

	/*
		@todo fix it!
	*/
	limit, _ := decimal.NewFromString("1000")
	left, _ := decimal.NewFromString("265.02")
	convertedLeft, _ := decimal.NewFromString("2.90422")

	if user.KYCPassed == false {
		return api.SuccessResponse(
			vbPresenters.NewDepositNotVerified(
				fmt.Sprintf("/qr/qr?content=%s&size=200", address.GetAddress()),
				address.GetAddress(),
				availableUntil,
				limit,
				methodParams.Currency,
				left,
				convertedLeft,
				"eur",
			),
		), nil
	}

	return api.SuccessResponse(
		vbPresenters.NewDeposit(
			fmt.Sprintf("/qr/qr?content=%s&size=200", address.GetAddress()),
			address.GetAddress(),
			&availableUntil,
			true,
			&minLimit,
		),
	), nil
}

func (h *VbHandler) isRegistered(legacyAsset entities.Asset, assetContractRepository assetcontract.Repository, address string) (bool, error) {
	dataControllerContract, err := assetContractRepository.Get(legacyAsset.ID, contracttype.DataController)
	if err != nil || dataControllerContract == nil {
		h.app.Logger().Error("HandleDeposit, cannot get datacontroller contract address",
			zap.Int64("assetId", legacyAsset.ID),
			zap.Error(err),
		)

		return false, errors.New("Cannot get dataController contract")
	}

	registered, err := h.blockchainReaderService.DataControllerIsRegisteredAddress(dataControllerContract.Address, address)
	if err != nil {
		h.app.Logger().Error("HandleDeposit, blockchainReaderService.DataControllerIsRegisteredAddress",
			zap.String("contract", dataControllerContract.Address),
			zap.String("address", address),
			zap.Error(err),
		)

		return false, err
	}

	if !registered {
		h.app.Logger().Info("address not registered in dataController", zap.String("address", address), zap.String("contract", dataControllerContract.Address))

		// return false, nil
	}

	/*
		emissionProviderContract, err := assetContractRepository.Get(legacyAsset.ID, contracttype.EmissionProvider)
		if err != nil || emissionProviderContract == nil {
			h.app.Logger().Error("HandleDeposit, cannot get emissionProvider contract address",
				zap.Int64("assetId", legacyAsset.ID),
				zap.Error(err),
			)

			return false, errors.New("Cannot get emissionProvider contract")
		}

		registered, err = h.blockchainReaderService.EmissionProviderIsRegisteredUser(emissionProviderContract.Address, address)
		if err != nil {
			h.app.Logger().Error("HandleDeposit, blockchainReaderService.EmissionProviderIsRegisteredUser",
				zap.String("contract", emissionProviderContract.Address),
				zap.String("address", address),
				zap.Error(err),
			)

			return false, err
		}

		if !registered {
			h.app.Logger().Info("address not registered in emissionProvider", zap.String("address", address), zap.String("contract", emissionProviderContract.Address))
		}
	*/
	return registered, nil
}

func (h *VbHandler) depositRentBMxAddress(pool interfaces.Pool, currency types.Currency, legacyAsset entities.Asset, userID int64, details map[string]interface{}) error {
	tenYearsDuration := time.Hour * 24 * 365 * 10

	sqlTx := h.app.DBConnection().MustBegin()

	address, err := pool.Rent(userID, currency, addresspurpose.VbDeposit, details, tenYearsDuration, sqlTx)
	if err != nil || address == nil {
		sqlTx.Rollback()
		h.app.Logger().Error("HandleDeposit, pool.Rent", zap.Int64("userID", userID), zap.Any("currency", currency), zap.Error(err))

		return err
	}

	sqlTx.Commit()

	if _, err := h.addressControlService.ProcessHolderAdding(userID, legacyAsset.ID, address.GetAddress()); err != nil {
		h.app.Logger().Error("HandleDeposit, addressControlService.ProcessHolderAdding", zap.Int64("userID", userID), zap.Error(err))

		return err
	}
	/*
		if err = h.addressControlService.ProcessUserAdding(userID, legacyAsset.ID, address.GetAddress()); err != nil {
			h.app.Logger().Error("HandleDeposit, addressControlService.ProcessUserAdding", zap.Int64("userID", userID), zap.Error(err))

			return err
		}
	*/
	return nil
}

// HandleWithdrawData ...
func (h *VbHandler) HandleWithdrawData(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	assets, err := h.vbAssetsService.GetAllEnabled()
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetAllEnabled", zap.Error(err))
		return api.InternalServerError(), nil
	}

	withdrawableAssets := make([]string, len(assets))

	for i, a := range assets {
		if a.CanWithdraw && a.Name != nil {
			withdrawableAssets[i] = string(*a.Name)
		}
	}

	methodParams := vbParams.NewWithdrawDataParams(withdrawableAssets)
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	isReceiverIsConnectedWallet := false
	if methodParams.WalletCurrency == "t20" {
		isReceiverIsConnectedWallet = true
	}

	asset := vb.Asset(methodParams.WalletCurrency)
	assetItem, err := h.vbAssetsService.GetByName(asset)
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetByName", zap.Any("asset", asset), zap.Error(err))
		return api.InternalServerError(), nil
	}

	if assetItem.WithdrawalFeeType == types.FeePercentage {
		h.app.Logger().Error("current fee type processing not implemented", zap.Any("asset", asset))
		return api.InternalServerError(), nil
	}

	currency := adapters.AssetToCurrency(asset)

	return api.SuccessResponse(vbPresenters.NewWithdrawData(assetItem, currency, isReceiverIsConnectedWallet)), nil
}

// HandleWithdrawCancel ...
func (h *VbHandler) HandleWithdrawCancel(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	methodParams := vbParams.WithdrawCancelParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	operation, err := h.operationRepo.Get(methodParams.OperationID)
	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, methodParams, "cannot get data", nil)
	}

	if operation.UserID != user.ID {
		return api.ErrorResponse(map[string]string{"transaction": "not found"}), nil
	}

	sqlTx, err := h.pauseRepo.Begin()
	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, methodParams, "cannot get data", nil)
	}

	item, err := h.pauseRepo.GetWithLock(sqlTx, operation.ID)
	if err != nil {
		sqlTx.Rollback()

		return nil, bfgerrors.NewApiIntErr(err, methodParams, "cannot get pause item", nil)
	}

	if item.Status != vbtypes.PauseInitialized {
		sqlTx.Rollback()

		return api.ErrorResponse(map[string]string{"transaction": "cannot cancel withdraw"}), nil
	}

	if err := h.pauseRepo.Cancel(sqlTx, operation.ID); err != nil {
		sqlTx.Rollback()

		return nil, bfgerrors.NewApiIntErr(err, methodParams, "cannot cancel withdraw", nil)
	}

	if err := sqlTx.Commit(); err != nil {
		return nil, bfgerrors.NewApiIntErr(err, methodParams, "cannot cancel withdraw", nil)
	}

	if err := h.vbQueueClient.PublishRollbackMessage(context.TODO(), methodParams.OperationID); err != nil {
		h.app.Logger().Error(
			"h.vbQueueClient.PublishRollbackMessage, should be rollbacked manualy",
			zap.Int64("operationId", methodParams.OperationID),
			zap.Error(err),
		)
	}

	return api.SuccessResponse(true), nil
}

// HandleWithdraw ...
func (h *VbHandler) HandleWithdraw(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	// проверка адресов для вывода - некоторые валюты можно выводить только на привязанные адреса
	// нужно будет проверить что тот адрес, который введен, привязан
	// на MVP вывод только на эфир

	assets, err := h.vbAssetsService.GetAllEnabled()
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetAllEnabled", zap.Error(err))
		return api.InternalServerError(), nil
	}

	withdrawableAssets := make([]string, len(assets))

	for i, a := range assets {
		if a.CanWithdraw && a.Name != nil {
			withdrawableAssets[i] = string(*a.Name)
		}
	}

	methodParams := vbParams.NewWithdrawParams(withdrawableAssets)
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	currency := vb.Asset(methodParams.Currency)

	if currency == vbtypes.ETH || currency == vbtypes.BMC || vbtypes.IsBMxToken(currency) {
		methodParams.ReceiveAddress = strings.ToLower(methodParams.ReceiveAddress)
	}

	inBlackList, err := h.ethAddressService.CheckInBlacklist(methodParams.ReceiveAddress)
	if err != nil {
		h.app.Logger().Error("HandleWithdraw can not check eth address in black list",
			zap.Int64("userId", user.ID),
			zap.Any("currency", currency),
			zap.Error(err),
		)
		return api.InternalServerError(), nil
	}

	if inBlackList {
		h.app.Logger().Warn("HandleWithdraw trying to withdraw to blacklist address",
			zap.Int64("userId", user.ID),
			zap.Any("currency", currency),
			zap.Any("address", methodParams.ReceiveAddress),
		)

		error := map[string]string{
			"ReceiveAddress": "invalid Ethereum address",
		}

		return api.ErrorResponse(error), nil
	}

	balance, err := h.vbBalanceService.GetBalanceByAsset(user.ID, currency)
	if err != nil {
		h.app.Logger().Error("HandleWithdraw can not get balance by asset",
			zap.Int64("userId", user.ID),
			zap.Any("currency", currency),
			zap.Error(err),
		)
		return api.InternalServerError(), nil
	}

	denormalizedBalance := vbtypes.Denormalize(*balance)

	if denormalizedBalance.LessThan(methodParams.Amount) {
		return api.ErrorResponse(types.MapI{"amount": "Insufficient funds"}), nil
	}

	transactions, err := h.transactionsStrategy.GetList(vbtypes.OperationWithdraw, &currency)
	if err != nil {
		h.app.Logger().Error("transactionsStrategy.GetList", zap.Error(err))
		return api.InternalServerError(), nil
	}

	operation := operations.New(h.operationRepo, transactions, h.vbBalanceService, h.app.Logger())

	withdrawalFee, err := h.vbAssetsService.GetWithdrawalFee(currency)
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetWithdrawalFee", zap.Any("currency", currency), zap.Error(err))
		return api.InternalServerError(), nil
	}

	if withdrawalFee.Type == types.FeePercentage {
		h.app.Logger().Error("not implemented")
		return api.InternalServerError(), nil
	}

	operationParams := operations.WithdrawCryptoParams{
		ReceiveAddress: methodParams.ReceiveAddress,
		Fee:            withdrawalFee.DenormalizedValue,
	}

	amount := vbtypes.Normalize(methodParams.Amount)

	operationID, err := operation.Init(
		user.ID,
		vb.AssetAmount{Value: amount, Currency: currency},
		0,
		operationParams,
	)
	if err != nil {
		h.app.Logger().Error("operation.Init", zap.Error(err))
		return api.InternalServerError(), nil
	}

	if operationID != nil {
		if err := h.vbQueueClient.PublishPrepareMessage(context.TODO(), *operationID); err != nil {
			h.app.Logger().Error(
				"HandleSell, vbQueueClient.PublishPrepareMessage",
				zap.Int64("operationID", *operationID),
				zap.Error(err),
			)
		}
	}

	return api.SuccessResponse(vbPresenters.NewWithdraw()), nil
}

// HandleTransactions ...
func (h *VbHandler) HandleTransactions(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	methodParams := vbParams.TransactionsParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	user, err := api.GetUserFromContext(req)

	if err != nil {
		h.app.Logger().Warn("api.HandleTransactions cannot get user",
			zap.Error(err))
		return api.AccessDeniedResponse("user not authorized"), nil
	}

	userTransactions, err := h.vbBalanceService.UserTransactions(user.ID)

	emptyResponse := make([]vbEntities.BalanceTransaction, 0)

	if err != nil {
		h.app.Logger().Error("api.HandleTransactions cannot get user transactions",
			zap.Error(err))
		return api.SuccessResponse(emptyResponse), nil
	}

	if userTransactions == nil {
		return api.SuccessResponse(emptyResponse), nil
	}

	forRequestCurrencies := make([]string, 0)
	processedCurrencies := make(map[string]bool)

	for _, transaction := range userTransactions {
		symbol := adapters.BmxToRatesString(transaction.BalanceAsset)
		if ok := processedCurrencies[symbol]; !ok {
			processedCurrencies[symbol] = true
			forRequestCurrencies = append(forRequestCurrencies, symbol)
		}
	}

	displayCurrency := types.Currency(methodParams.DisplayCurrency)

	rates, err := h.currencyRatesService.GetCachedReturns(req.Context(), forRequestCurrencies)

	if err != nil {
		h.app.Logger().Error("api.HandleTransactions cannot get rates",
			zap.Error(err))

		return api.SuccessResponse(emptyResponse), nil
	}

	if rates == nil {
		return api.SuccessResponse(emptyResponse), nil
	}

	result, err := vbPresenters.NewTransactionsList(userTransactions, *rates, displayCurrency)

	if err != nil {
		h.app.Logger().Error("api.HandleTransactions cannot get NewTransactionsList",
			zap.Error(err))

		return api.SuccessResponse(emptyResponse), nil
	}

	return api.SuccessResponse(result), nil

}

// HandleBalance ...
func (h *VbHandler) HandleBalance(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	methodParams := vbParams.BalanceParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	quote := *spHelper.DisplayCurrencyToAsset(methodParams.DisplayCurrency)

	user, err := api.GetUserFromContext(req)

	if err != nil {
		h.app.Logger().Warn("api.GetUserFromContex",
			zap.Error(err))
		return api.AccessDeniedResponse(map[string]string{"error": "user is not authorized"}), nil
	}

	balance, err := h.vbBalanceService.UserBalanceInCurrency(user.ID, quote)

	if err != nil {
		h.app.Logger().Error("HandleAssets,  get balance error",
			zap.Error(err))
		return api.SuccessResponse(vbPresenters.NewBalance(decimal.Zero, quote)), nil
	}

	//before fixing plates, set as balance
	fpb := vbtypes.Truncate(quote, balance)

	//return as fpb
	return api.SuccessResponse(vbPresenters.NewBalance(fpb, quote)), nil
}

// HandleAssets ...
func (h *VbHandler) HandleAssets(w http.ResponseWriter, req *http.Request) (*api.Response, error) {

	user, err := api.GetUserFromContext(req)

	if err != nil {
		return api.AccessDeniedResponse(map[string]string{"error": "user is not authorized"}), nil
	}

	country, err := h.getUserCountry(req)

	if err != nil {
		return nil, err
	}

	assets, err := h.legacyAssetService.GetVisibleAssets(*country)
	if err != nil {
		h.app.Logger().Error("HandleAssets, unable to get assets", zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to get assets", nil)
	}

	accessableAssetsSymbols := presenters.NewSymbolAssets(assets)

	vbAssets, err := h.vbAssetsService.GetAllEnabled()

	if err != nil {
		return api.ErrorResponse(map[string]string{"error": "unable to get assets"}), nil
	}

	filtedAssets := make([]vbEntities.VbAsset, 0)

	for _, asset := range vbAssets {
		if asset.RoutingKey == nil {
			filtedAssets = append(filtedAssets, asset)
			continue
		}

		for _, s := range accessableAssetsSymbols {
			if s == *asset.RoutingKey {
				filtedAssets = append(filtedAssets, asset)
			}
		}
	}

	assetBalances, err := h.vbBalanceService.UserBalance(user.ID)

	if err != nil {
		return api.ErrorResponse(map[string]string{"error": "unable to get assets balance"}), nil
	}

	result, err := vbPresenters.NewAssetsList(filtedAssets, assetBalances)

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to get assetsList", nil)
	}

	return api.SuccessResponse(result), nil
}

// HandleAssetsPrice ...
func (h *VbHandler) HandleAssetsPrice(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	filteredAssets, err := h.getUserAssets(req)
	if err != nil {
		h.app.Logger().Error("h.HandleAssetsPrice getUserAssets, error",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get assets"}), nil
	}

	srcCurrencies, err := h.getAssetsNames(filteredAssets)
	if err != nil {
		h.app.Logger().Error("h.HandleAssetsPrice getAssetsNames, error",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get names"}), nil
	}

	rates, err := h.currencyRatesService.GetCachedReturns(req.Context(), srcCurrencies)
	if err != nil {
		h.app.Logger().Error("HandleAssetsPrice, currencyRatesService.GetCachedReturns, cannot get cached returns",
			zap.Any("srcCurrencies", srcCurrencies),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get prices"}), nil
	}

	return api.SuccessResponse(rates), nil
}

// HandleTradings ...
func (h *VbHandler) HandleTradings(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	assets, err := h.getUserAssets(req)
	if err != nil {
		h.app.Logger().Error("h.HandleTradings getUserAssets, error",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get assets"}), nil
	}

	srcCurrencies, err := h.getAssetsNames(assets)
	if err != nil {
		h.app.Logger().Error("h.HandleAssetsPrice getAssetsNames, error",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get names"}), nil
	}

	rates, err := h.currencyRatesService.GetCachedReturns(req.Context(), srcCurrencies)
	if err != nil {
		h.app.Logger().Error("HandleAssetsPrice, currencyRatesService.GetCachedReturns, cannot get cached returns",
			zap.Any("srcCurrencies", srcCurrencies),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get prices"}), nil
	}

	sparklines, err := h.currencyRatesService.GetCachedSparklines(req.Context(), srcCurrencies)
	if err != nil {
		h.app.Logger().Error("HandleAssetsPrice, currencyRatesService.GetCachedSparklines, cannot get cached sparklines",
			zap.Any("srcCurrencies", srcCurrencies),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get prices"}), nil
	}

	marketMakerVolume24h, err := h.spotwareApiService.TradeVolume()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, spotwareApiService.TradeVolume, cannot get marketmaker volumes",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get mm volume"}), nil
	}

	ordersVolume24h, err := h.ordersService.TradingVolumeBySymbolID()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, ordersService.Volume24h, cannot get volumes",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get volume"}), nil
	}

	assetsMap, err := h.spotwareApiService.AssetsAsMap()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, spotwareApiService.AssetsAsMap, cannot get assetsMap",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get symbols map"}), nil
	}

	symbolsMap, err := h.spotwareApiService.SymbolsAsMap()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, spotwareApiService.SymbolsAsMap, cannot get symbolsMap",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get symbols map"}), nil
	}

	volumesMainpageAdapter := tradingvolumes.NewMainPageAdapter(ordersVolume24h, marketMakerVolume24h, assetsMap, symbolsMap)
	if volumesMainpageAdapter == nil {
		h.app.Logger().Error("HandleVolume24h, tradingvolumes.NewMainPageAdapter, cannot get mainpage adapter")
		return api.ErrorResponse(map[string]string{"error": "unable to get main page adapter"}), nil
	}

	volumes, err := volumesMainpageAdapter.Transform()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, volumesMainpageAdapter.Transform, cannot transform volumes to main page map",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to transform volumes"}), nil
	}

	result := vbPresenters.NewTradings(assets, *rates, *sparklines, volumes)

	return api.SuccessResponse(result), nil
}

// HandleSparklines ...
func (h *VbHandler) HandleSparklines(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	assets, err := h.getUserAssets(req)
	if err != nil {
		h.app.Logger().Error("h.HandleSparklines getUserAssets, error",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get assets"}), nil
	}

	srcCurrencies, err := h.getAssetsNames(assets)
	if err != nil {
		h.app.Logger().Error("h.HandleSparklines getAssetsNames, error",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get names"}), nil
	}

	sparklines, err := h.currencyRatesService.GetCachedSparklines(req.Context(), srcCurrencies)
	if err != nil {
		h.app.Logger().Error("HandleSparklines, currencyRatesService.GetCachedSparklines, cannot get cached sparklines",
			zap.Any("srcCurrencies", srcCurrencies),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get sparklines"}), nil
	}

	return api.SuccessResponse(sparklines), nil
}

// HandleVolumes ...
func (h *VbHandler) HandleVolumes(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	marketMakerVolume24h, err := h.spotwareApiService.TradeVolume()
	if err != nil {
		h.app.Logger().Error("handleVolumes, spotwareApiService.TradeVolume, cannot get marketmaker volumes",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get mm volume"}), nil
	}

	ordersVolume24h, err := h.ordersService.TradingVolumeBySymbolID()
	if err != nil {
		h.app.Logger().Error("handleVolumes, ordersService.Volume24h, cannot get volumes",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get volume"}), nil
	}

	assetsMap, err := h.spotwareApiService.AssetsAsMap()
	if err != nil {
		h.app.Logger().Error("handleVolumes, spotwareApiService.AssetsAsMap, cannot get assetsMap",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get symbols map"}), nil
	}

	symbolsMap, err := h.spotwareApiService.SymbolsAsMap()
	if err != nil {
		h.app.Logger().Error("handleVolumes, spotwareApiService.SymbolsAsMap, cannot get symbolsMap",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get symbols map"}), nil
	}

	volumesMainpageAdapter := tradingvolumes.NewMainPageAdapter(ordersVolume24h, marketMakerVolume24h, assetsMap, symbolsMap)
	if volumesMainpageAdapter == nil {
		h.app.Logger().Error("handleVolumes, tradingvolumes.NewMainPageAdapter, cannot get mainpage adapter")
		return api.ErrorResponse(map[string]string{"error": "unable to get main page adapter"}), nil
	}

	volumes, err := volumesMainpageAdapter.Transform()
	if err != nil {
		h.app.Logger().Error("handleVolumes, volumesMainpageAdapter.Transform, cannot transform volumes to main page map",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to transform volumes"}), nil
	}

	return api.SuccessResponse(volumes), nil
}

func (h *VbHandler) getUserAssets(req *http.Request) ([]vbEntities.VbAsset, error) {
	user, err := api.GetUserFromContext(req)

	if err != nil {
		return nil, err
	}

	isLoggedIn := user != nil
	country, err := h.getUserCountry(req)

	if err != nil {
		return nil, err
	}

	assets, err := h.legacyAssetService.GetVisibleAssets(*country)
	if err != nil {
		h.app.Logger().Error("HandleAssets, unable to get assets", zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to get assets", nil)
	}

	accessableAssetsSymbols := presenters.NewSymbolAssets(assets)

	vbAssets, err := h.vbAssetsService.GetAllEnabled()

	filtedAssets := make([]vbEntities.VbAsset, 0)

	for _, asset := range vbAssets {
		if asset.RoutingKey == nil {
			filtedAssets = append(filtedAssets, asset)
			continue
		}

		if asset.KycOnly && (!isLoggedIn || user.KYCPassed == false) {
			continue
		}

		for _, s := range accessableAssetsSymbols {
			if s == *asset.RoutingKey {
				filtedAssets = append(filtedAssets, asset)
			}
		}
	}

	return filtedAssets, nil
}

func (h *VbHandler) getAssetsNames(assets []vbEntities.VbAsset) ([]string, error) {
	result := make([]string, len(assets))

	vbAssetsByName := map[string]vbEntities.VbAsset{}

	for i, a := range assets {
		if a.Name == nil {
			return nil, errors.New("Asset name can not be null")
		}
		vbAssetsByName[string(*a.Name)] = a

		var from string

		if a.RoutingKey == nil {
			from = strings.ToLower(string(*a.Name))
		} else {
			from = *a.RoutingKey
		}

		result[i] = from
	}

	return result, nil
}

// HandleVolume24h ...
func (h *VbHandler) HandleVolume24h(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	marketMakerVolume24h, err := h.spotwareApiService.TradeVolume()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, spotwareApiService.TradeVolume, cannot get marketmaker volumes",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get mm volume"}), nil
	}

	ordersVolume24h, err := h.ordersService.TradingVolumeBySymbolID()
	if err != nil {
		h.app.Logger().Error("HandleVolume24h, ordersService.Volume24h, cannot get volumes",
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get volume"}), nil
	}

	return api.SuccessResponse(vbPresenters.NewVolume24H(ordersVolume24h, marketMakerVolume24h)), nil
}

// HandlePrice ...
func (h *VbHandler) HandlePrice(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	// TODO: check access to asset
	methodParams := vbParams.AssetPriceParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	assetID := vb.Asset(methodParams.From)

	asset, err := h.vbAssetsService.GetByName(assetID)

	if err != nil {
		h.app.Logger().Error("HandlePrice, vbAssetsService.GetByName, cannot get asset ",
			zap.Any("assetID", assetID),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get asset"}), nil
	}

	if asset.KycOnly && user.KYCPassed == false {
		return api.ErrorResponse(map[string]string{"error": "not have access"}), nil
	}

	var from string

	if asset.RoutingKey != nil {
		from = *asset.RoutingKey
	} else {
		from = strings.ToLower(string(*asset.Name))
	}

	rate, err := h.currencyRatesService.GetRate(req.Context(), types.Currency(from), types.Currency(methodParams.To))

	if err != nil {
		h.app.Logger().Error("HandlePrice, currencyRatesService.GetRate, cannot get prices ",
			zap.Any("bases", methodParams.From),
			zap.Error(err))
		return api.ErrorResponse(map[string]string{"error": "unable to get prices"}), nil
	}

	price := vbEntities.AssetPrice{
		Asset: assetID,
		Price: decimal.NewFromFloat(rate),
	}

	result, err := vbPresenters.NewAssetPrice(price)

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to prepare presenters", nil)
	}

	return api.SuccessResponse(result), nil
}

// HandleCreateDepositForm ...
func (h *VbHandler) HandleCreateDepositForm(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	isChinaPerson, err := h.countryPersonDetectorService.IsChinaPerson(user)

	if isChinaPerson {
		return api.AccessDeniedResponse("Access denied"), nil
	}

	isUsPerson, err := h.countryPersonDetectorService.IsUsPerson(user)

	if isUsPerson {
		return api.AccessDeniedResponse("Access denied"), nil
	}

	methodParams := vbParams.CreateCardFormParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	var unverifiedFiatLimitEUR = decimal.NewFromFloat(defaultLimitEUR)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	//start fiat limit verifying for unverified user
	if !user.KYCPassed {

		balance, err := h.vbBalanceService.UserBalanceInCurrency(user.ID, vbtypes.EUR)

		if err != nil {
			h.app.Logger().Error("HandleCreateCardForm,  get balance error",
				zap.Error(err))
			return api.SuccessResponse(vbPresenters.NewBalance(decimal.Zero, vbtypes.EUR)), nil
		}

		h.app.Logger().Info("HandleCreateCardForm,  balance",
			zap.Int64("userId", user.ID),
			zap.Any("balance", balance))

		eurLeft := unverifiedFiatLimitEUR.Sub(balance)

		if eurLeft.IsNegative() {
			eurLeft = decimal.Zero
		}

		eurAmount := decimal.Zero
		//convert user amount to eur
		amountRate, err := h.currencyRatesService.GetRate(req.Context(), types.Currency(strings.ToLower(methodParams.Currency)), currencies.EUR)
		if err != nil {
			return nil, bfgerrors.NewApiIntErr(err, methodParams, "unable to get currency rates", nil)
		}

		eurAmount = decimal.NewFromFloat(amountRate).Mul(methodParams.Amount)

		isLimitExceed := eurAmount.GreaterThan(eurLeft)

		h.app.Logger().Info("HandleCreateCardForm, unverified purchase, order amounts info",
			zap.Int64("userID", user.ID),
			zap.Any("eurLeft", eurLeft),
			zap.Any("orderAmount", methodParams.Amount),
			zap.Any("orderCurrency", methodParams.Currency),
			zap.Any("orderEurAmount", eurAmount),
			zap.Bool("isLimitExceed", isLimitExceed),
			zap.Error(err))

		if isLimitExceed {
			convertedCurrency := decimal.NewFromFloat(amountRate).Mul(eurLeft)

			errorMessages := types.MapI{"amount": "limit is exceeded, user verification needed"}
			errorData := types.MapI{"amount": vbPresenters.NewDepositFiatNotVerified(
				unverifiedFiatLimitEUR, strings.ToLower(methodParams.Currency),
				eurLeft, convertedCurrency, string(currencies.EUR)),
			}
			return api.ErrorResponseWithData(errorMessages, errorData), nil
		}
	}

	result, err := h.cardsService.Create(nil, user.ID, methodParams.Amount, vb.Asset(methodParams.Currency), ecpTypes.PaymentTypeDebitCard, nil)

	if err != nil {
		h.app.Logger().Error("HandleCreateCardForm, cannot create form",
			zap.Any("params", methodParams),
			zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to create form", nil)
	}

	return api.SuccessResponse(*result), nil

}

// HandleFormResponse ...
func (h *VbHandler) HandleFormResponse(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	vars := req.URL.Query()

	methodParams, err := vbParams.NewFormResponseParams(vars)

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unknown params for form response", types.MapI{"url": req.URL})
	}

	payment, err := h.cardsService.Get(user.ID, methodParams.FormID)

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "can not check payment", types.MapI{"url": req.URL})
	}

	if payment == nil {
		h.app.Logger().Error("HandleFormResponse, form not isset",
			zap.Any("params", methodParams),
			zap.Int64("userID", user.ID),
			zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, nil, "unknown payment", types.MapI{"url": req.URL})
	}

	var isSuccess *bool

	isProcessed := h.cardsService.IsPaymentProcessed(*payment)

	if isProcessed {
		isSuccess = &payment.IsSuccess
	} else {
		isSuccess, err = h.cardsService.ReceivePaymentDataOnce(user.ID, methodParams.FormID)

		if err != nil {
			return nil, bfgerrors.NewApiIntErr(err, nil, "can not receive payment data", types.MapI{"url": req.URL})
		}
	}

	redirectTo := "/dashboard#error"

	if *isSuccess == true {
		go func() {
			switch payment.PaymentType {
			case ecpTypes.PaymentTypeDebitCard:
				// TODO: remove it
				if user.ID == 1003587 {
					h.app.Logger().Warn("User 1003587 have deposit", zap.Any("formID", methodParams.FormID))
					return
				}

				msg := cardTypes.PaymentConfirmation{
					UserID: user.ID,
					FormID: methodParams.FormID,
				}
				err := h.cardsAmqpClient.PublishFiatPaymentConfirmMessage(msg)

				if err != nil {
					h.app.Logger().Error("PublishFiatPaymentConfirmMessage,  message publishing error",
						zap.Any("params", methodParams),
						zap.Any("msg", msg),
						zap.Int64("userID", user.ID),
						zap.Error(err))
				}
			case ecpTypes.PaymentTypeCreditCard:
				if err := h.vbQueueClient.PublishPrepareMessage(context.TODO(), *payment.OperationId); err != nil {
					h.app.Logger().Error(
						"HandleDepositFormResponse, vbQueueClient.PublishPrepareMessage",
						zap.Int64("operationID", *payment.OperationId),
						zap.Error(err),
					)
				}
			}
		}()

		switch payment.PaymentType {
		case ecpTypes.PaymentTypeDebitCard:
			redirectTo = "/dashboard#success"
		case ecpTypes.PaymentTypeCreditCard:
			redirectTo = "/dashboard#success-withdraw"
		}
	}

	// TODO: remove it
	if user.ID == 1003587 {
		redirectTo = "/dashboard#error"
	}

	http.Redirect(w, req, h.baseURL+redirectTo, http.StatusFound)

	return nil, nil
}

// HandleCreateWithdrawForm ...
func (h *VbHandler) HandleCreateWithdrawForm(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user := api.MustGetUserFromContext(req)

	isChinaPerson, err := h.countryPersonDetectorService.IsChinaPerson(user)

	if isChinaPerson || user.KYCPassed == false {
		return api.AccessDeniedResponse("Access denied"), nil
	}

	methodParams := vbParams.CreateCardFormParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	currency := vb.Asset(methodParams.Currency)
	minAmount, err := h.vbAssetsService.GetMinimumWithdrawal(currency)
	if err != nil {
		h.app.Logger().Error("vbAssetsService.GetMinimumWithdrawal", zap.Error(err))
		return api.InternalServerError(), nil
	}

	if methodParams.Amount.LessThan(minAmount) {
		return api.ErrorResponse(types.MapI{"amount": "Amount should be greater minAmount"}), nil
	}

	transactions, err := h.transactionsStrategy.GetList(vbtypes.OperationWithdraw, &currency)
	if err != nil {
		h.app.Logger().Error("transactionsStrategy.GetList", zap.Error(err))
		return api.InternalServerError(), nil
	}

	balance, err := h.vbBalanceService.GetBalanceByAsset(user.ID, currency)
	if err != nil {
		h.app.Logger().Error("HandleCreateWithdrawForm can not get balance by asset",
			zap.Int64("userId", user.ID),
			zap.Any("currency", currency),
			zap.Error(err),
		)
		return api.InternalServerError(), nil
	}

	denormalizedBalance := vbtypes.Denormalize(*balance)

	if denormalizedBalance.Sub(methodParams.Amount).IsNegative() {
		return api.ErrorResponse(types.MapI{"amount": "Not enough balance"}), nil
	}

	requestID := h.generateWithdrawRequestId(user.ID)

	operation := operations.New(h.operationRepo, transactions, h.vbBalanceService, h.app.Logger())
	operationParams := operations.WithdrawCardParams{
		RequestID: requestID,
	}

	amount := vbtypes.Normalize(methodParams.Amount)

	operationID, err := operation.Init(
		user.ID,
		vb.AssetAmount{Value: amount, Currency: currency},
		0,
		operationParams,
	)
	if err != nil {
		h.app.Logger().Error("operation.Init", zap.Error(err))
		return api.InternalServerError(), nil
	}

	if operationID != nil {
		if err := h.vbQueueClient.PublishPrepareMessage(context.TODO(), *operationID); err != nil {
			h.app.Logger().Error(
				"HandleSell, vbQueueClient.PublishPrepareMessage",
				zap.Int64("operationID", *operationID),
				zap.Error(err),
			)
		}
	}
	return api.SuccessResponse(types.MapI{
		"requestId": requestID,
	}), nil
}

func (h *VbHandler) generateWithdrawRequestId(userID int64) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	return fmt.Sprintf("%v.%v.%v", time.Now().UnixNano(), r.Int63(), userID)
}

// HandleAssetByRouteKey ...
func (h *VbHandler) HandleAssetByRouteKey(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	user, err := api.GetUserFromContext(req)

	if err != nil {
		return api.AccessDeniedResponse(map[string]string{"error": "user is not authorized"}), nil
	}

	methodParams := vbParams.AssetByKeyParams{}
	params.MustDecodeParams(req.Body, &methodParams)

	if validationErrors := params.MustValidateParams(&methodParams); validationErrors != nil {
		return api.ErrorResponse(validationErrors), nil
	}

	routeKeyAsset, err := h.vbAssetsService.GetByRouteKey(methodParams.AssetID)

	if routeKeyAsset == nil {
		return api.SuccessResponse(nil), nil
	}

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to get asset", nil)
	}

	if routeKeyAsset.Name == nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to get asset name cannot be empty", nil)
	}

	cashAssets, err := h.vbAssetsService.GetByNames(routeKeyAsset.Cash)

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to get asset cash", nil)
	}
	//get user asset balances
	assetBalances, err := h.vbBalanceService.UserBalance(user.ID)
	//sort by assets balances
	balanceSortedByName := map[string]vbEntities.AssetBalance{}
	for _, ab := range assetBalances {
		balanceSortedByName[string(ab.Asset)] = ab
	}
	if err != nil {
		return api.ErrorResponse(map[string]string{"error": "unable to get assets balances"}), nil
	}

	result, err := vbPresenters.NewAssetWithCash(*routeKeyAsset, balanceSortedByName[string(*routeKeyAsset.Name)], *cashAssets, assetBalances)

	if err != nil {
		return nil, bfgerrors.NewApiIntErr(err, nil, "unable to prepare asset", nil)
	}

	return api.SuccessResponse(result), nil
}

// HandleSymbols ...
func (h *VbHandler) HandleSymbols(w http.ResponseWriter, req *http.Request) (*api.Response, error) {
	data, err := h.spotwareApiService.SymbolsAsMap()
	if err != nil {
		h.app.Logger().Error("SymbolsAsMap", zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, nil, "HandleSymbols", nil)
	}

	return api.SuccessResponse(vbPresenters.NewSymbols(data)), nil
}

// TODO: refactor this copy/paster
func (h *VbHandler) getUserCountry(req *http.Request) (*string, error) {
	country := "none" //set default country as none
	countryA2FromHeaders := req.Header.Get("x-client-country2")

	if len(countryA2FromHeaders) > 0 {
		country = strings.ToLower(countryA2FromHeaders)
	}

	h.app.Logger().Info("getUserCountry",
		zap.String("method", req.Method),
		zap.String("remoteAddr", req.RemoteAddr),
		zap.String("host", req.Host),
		zap.Any("headers", req.Header),
	)
	isLoggedIn, err := api.IsLoggedIn(req)

	if err != nil {
		h.app.Logger().Error("getUserCountry, IsLoggedIn", zap.Error(err))
		return nil, bfgerrors.NewApiIntErr(err, nil, "getUserCountry, IsLoggedIn", nil)
	}

	if isLoggedIn {

		user, err := api.GetCurrentUser(req, h.usersRepository)

		if err != nil {
			panic(errors.Wrap(err, "getUserCountry, GetCurrentUser"))
		}

		country = strings.ToLower(user.Country)

		isUsPerson, err := h.countryPersonDetectorService.IsUsPerson(user)

		if err != nil {
			h.app.Logger().Error("getUserCountry, IsUsPerson",
				zap.Int64("userId", user.ID),
				zap.Error(err))
			return nil, bfgerrors.NewApiIntErr(err, nil, "getUserCountry, IsUsPerson", nil)
		}

		if isUsPerson {
			country = countryperson.UsCountry2Code
		}

	}

	return &country, nil
}

// New ...
func New(application services.App,
	currencyRatesService currencyrates.Service,
	operationRepo operations.Repo,
	pauseRepo pauseRepository.Repo,
	transactionsStrategy transactions.Strategy,
	vbQueueClient queue.VirtualBalanceQueueClient,
	vbAssetsService assets.Service,
	poolRegistry cyclicAddressInterfaces.PoolRegistry,
	vbBalanceService vbBalance.Service,
	legacyAssetService legacyAssetService.Service,
	countryPersonDetectorService countryperson.DetectorService,
	usersRepository users.Repository,
	addressControlService addresscontrol.Service,
	blockchainReaderService blockchain.ReaderService,
	cardsService cards.Service,
	cardsAmqpClient cards.AmqpClient,
	baseURL string,
	spotwareApiService spotware.ApiService,
	ethAddressService ethaddress.Service,
	ordersService *orders.Service,
) (*VbHandler, error) {
	if application == nil {
		return nil, errors.New("vbhandler.New, application must be not empty")
	}

	if currencyRatesService == nil {
		return nil, errors.New("vbhandler.New, currencyRatesService must be not empty")
	}

	if operationRepo == nil {
		return nil, errors.New("vbhandler.New, operationRepo cannot be empty")
	}

	if pauseRepo == nil {
		return nil, errors.New("vbhandler.New, pauseRepo cannot be empty")
	}

	if transactionsStrategy == nil {
		return nil, errors.New("vbhandler.New, transactionsStrategy cannot be empty")
	}

	if vbQueueClient == nil {
		return nil, errors.New("vbhandler.New, vbQueueClient cannot be empty")
	}

	if vbAssetsService == nil {
		return nil, errors.New("vbhandler.New, vbAssetsService cannot be empty")
	}

	if poolRegistry == nil {
		return nil, errors.New("vbhandler.New, poolRegistry cannot be empty")
	}

	if vbBalanceService == nil {
		return nil, errors.New("vbhandler.New, vbBalanceService cannot be empty")
	}

	if legacyAssetService == nil {
		return nil, errors.New("vbhandler.New, legacyAssetService cannot be empty")
	}

	if countryPersonDetectorService == nil {
		return nil, errors.New("vbhandler.New, countryPersonDetectorService cannot be empty")
	}

	if usersRepository == nil {
		return nil, errors.New("vbhandler.New, usersRepository cannot be empty")
	}

	if addressControlService == nil {
		return nil, errors.New("vbhandler.New, addressControlService cannot be empty")
	}

	if blockchainReaderService == nil {
		return nil, errors.New("vbhandler.New, blockchainReaderService cannot be empty")
	}

	if cardsService == nil {
		return nil, errors.New("vbhandler.New, cardsService cannot be empty")
	}

	if cardsAmqpClient == nil {
		return nil, errors.New("vbhandler.New, cardsAmqpClient cannot be empty")
	}

	if spotwareApiService == nil {
		return nil, errors.New("vbhandler.New, spotwareApiService cannot be empty")
	}

	if ethAddressService == nil {
		return nil, errors.New("vbhandler.New, ethAddressService cannot be empty")
	}

	if ordersService == nil {
		return nil, errors.New("vbhandler.New, ordersService cannot be empty")
	}

	return &VbHandler{
		app:                          application,
		currencyRatesService:         currencyRatesService,
		operationRepo:                operationRepo,
		pauseRepo:                    pauseRepo,
		transactionsStrategy:         transactionsStrategy,
		vbQueueClient:                vbQueueClient,
		vbAssetsService:              vbAssetsService,
		poolRegistry:                 poolRegistry,
		vbBalanceService:             vbBalanceService,
		legacyAssetService:           legacyAssetService,
		countryPersonDetectorService: countryPersonDetectorService,
		usersRepository:              usersRepository,
		addressControlService:        addressControlService,
		blockchainReaderService:      blockchainReaderService,
		cardsService:                 cardsService,
		cardsAmqpClient:              cardsAmqpClient,
		baseURL:                      baseURL,
		spotwareApiService:           spotwareApiService,
		ethAddressService:            ethAddressService,
		ordersService:                ordersService,
	}, nil
}
