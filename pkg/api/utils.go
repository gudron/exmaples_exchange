package api

import (
	"github.com/bfg-dev/crypto-core/pkg/entities"
	"github.com/bfg-dev/crypto-core/pkg/services/users"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"net"
	"net/http"
	"strings"
)

func MustGetUserFromContext(req *http.Request) *entities.User {
	user, err := GetUserFromContext(req)

	if err != nil {
		panic(errors.Wrap(err, "api.mustGetUserFromContext: unable to get current user from context"))
	}

	if user == nil {
		panic(errors.Wrap(err, "api.mustGetUserFromContext: user in context was not found"))
	}

	return user
}

func GetUserFromContext(req *http.Request) (*entities.User, error) {
	userCtx := req.Context().Value("user")
	user, ok := userCtx.(*entities.User)

	if ok != true {
		return nil, errors.New("api.getUserFromContext: unable to get user from context")
	}

	return user, nil
}

func MustGetSessionFromContext(req *http.Request) *sessions.Session {
	session, err := GetSessionFromContext(req)

	if err != nil {
		panic(errors.Wrap(err, "api.mustGetSessionFromContext"))
	}

	return session
}

func GetSessionFromContext(req *http.Request) (*sessions.Session, error) {
	session, ok := req.Context().Value("session").(*sessions.Session)

	if ok != true {
		return nil, errors.New("api.getSessionFromContext: unable to get session from context")
	}

	return session, nil
}

//GetCurrentUserID returns current user id or nil if not logged in
func GetCurrentUserID(req *http.Request) (*int64, error) {
	session, err := GetSessionFromContext(req)

	if err != nil {
		return nil, errors.Wrap(err, "api.getCurrentUserID unable to get session")
	}

	userIDAsString, ok := session.Values["userId"]

	if ok != true {
		return nil, nil
	}

	userID, ok := userIDAsString.(int64)

	if ok != true {
		return nil, errors.New("unable to convert userID to int64")
	}

	return &userID, nil
}

//MustIsLoggedIn strict version of IsLoggedIn
func MustIsLoggedIn(req *http.Request) bool {
	isLoggedIn, err := IsLoggedIn(req)

	if err != nil {
		panic(errors.Wrap(err, "api.mustIsLoggedIn"))
	}

	return isLoggedIn
}

//IsLoggedIn returns logged user or not
func IsLoggedIn(req *http.Request) (bool, error) {
	currentUserID, err := GetCurrentUserID(req)

	if err != nil {
		return false, errors.Wrap(err, "api.IsLoggedIn unable to get current user ID")
	}

	if currentUserID != nil {
		return true, nil
	}

	return false, nil
}

//GetCurrentUser returns current logged in user
func GetCurrentUser(req *http.Request, getter users.UserByIDGetter) (*entities.User, error) {
	userID, err := GetCurrentUserID(req)

	if err != nil {
		return nil, errors.Wrap(err, "api.getCurrentUser unable to get current user id")
	}

	if userID == nil {
		return nil, nil
	}

	user, err := getter.GetByID(*userID)

	if err != nil {
		return nil, errors.Wrap(err, "api.getCurrentUser unable to get current user by id from getter")
	}

	return user, nil
}

func GetUserIP(req *http.Request) (string, error) {
	ipFromHeaderRaw := req.Header.Get("x-client-ip")

	if len(ipFromHeaderRaw) > 0 {
		ipFromHeaderList := strings.Split(ipFromHeaderRaw, ",")
		ipFromHeader := strings.TrimSpace(ipFromHeaderList[0])

		return ipFromHeader, nil
	}

	host, _, err := net.SplitHostPort(req.RemoteAddr)

	if err != nil {
		return "", errors.Wrap(err, "api.GetUserIP, unable to split host and port")
	}

	return host, nil
}
