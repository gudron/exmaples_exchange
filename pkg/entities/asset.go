package entities

import (
	"time"

	"github.com/bfg-dev/crypto-core/pkg/types"
)

//Asset ...
type Asset struct {
	ID                  int64                          `db:"id"`
	CreatedAt           time.Time                      `db:"createdAt"`
	UpdatedAt           time.Time                      `db:"updatedAt"`
	Name                string                         `db:"name"`
	Symbol              string                         `db:"symbol"`
	Contract            string                         `db:"contract"`
	AssetSymbol         string                         `db:"assetSymbol"`
	Order               *int                           `db:"order"`
	DataSciencePrice    bool                           `db:"dataSciencePrice"`
	Liquidity           types.AssetLiquidityType       `db:"liquidity"`
	RestrictedCountries types.AssetRestrictedCountries `db:"restrictedCountries"`
	Currency            types.Currency                 `db:"currency"`
	KycOnly             bool                           `db:"kycOnly"`
	ReturnRisk          int16                          `db:"returnRisk"`
	ChangeDescription   string                         `db:"changeDescription"`
	InvestIn            string                         `db:"investIn"`
}

// Assets ...
type Assets []Asset

// MarshalToByIDMap ...
func (al *Assets) MarshalToByIDMap() map[int64]Asset {
	result := make(map[int64]Asset)

	for _, record := range *al {
		result[record.ID] = record
	}

	return result
}

// MarshalToBySymbolMap ...
func (al *Assets) MarshalToBySymbolMap() map[string]Asset {
	result := make(map[string]Asset)

	for _, record := range *al {
		result[record.Symbol] = record
	}

	return result
}

// GetSymbols fuct for getting assets symbols list from assets list ...
func (al *Assets) GetSymbols() []string {
	result := make([]string, len(*al))

	for i, record := range *al {
		result[i] = record.Symbol
	}

	return result
}

// GetIDs fuct for getting assets id list from assets list ...
func (al *Assets) GetIDs() []int64 {
	result := make([]int64, len(*al))

	for i, record := range *al {
		result[i] = record.ID
	}

	return result
}
