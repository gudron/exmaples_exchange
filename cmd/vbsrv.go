package cmd

import (
	"net/http"
	"time"

	"github.com/bfg-dev/crypto-core/pkg/api"
	"github.com/bfg-dev/crypto-core/pkg/api/middlewares"
	"github.com/bfg-dev/crypto-core/pkg/api/vbhandler"
	"github.com/bfg-dev/crypto-core/pkg/helpers/cmd"
	"github.com/bfg-dev/crypto-core/pkg/helpers/platform"
	"github.com/bfg-dev/crypto-core/pkg/services"
	servicesAmqp "github.com/bfg-dev/crypto-core/pkg/services/amqp"
	legacyAssetService "github.com/bfg-dev/crypto-core/pkg/services/asset"
	legacyAssetPostgres "github.com/bfg-dev/crypto-core/pkg/services/asset/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/bitgo"
	"github.com/bfg-dev/crypto-core/pkg/services/blockchain/addresscontrol"
	addressControlPostgres "github.com/bfg-dev/crypto-core/pkg/services/blockchain/addresscontrol/postgres"
	blockchainAMQP "github.com/bfg-dev/crypto-core/pkg/services/blockchain/amqp"
	assetContractPostgres "github.com/bfg-dev/crypto-core/pkg/services/blockchain/assetcontract/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/broadcastqueue"
	"github.com/bfg-dev/crypto-core/pkg/services/cards"
	cardsAqmp "github.com/bfg-dev/crypto-core/pkg/services/cards/amqp"
	"github.com/bfg-dev/crypto-core/pkg/services/cards/ecp"
	cardsHistoryRepo "github.com/bfg-dev/crypto-core/pkg/services/cards/history/postgres"
	cardsRepo "github.com/bfg-dev/crypto-core/pkg/services/cards/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/currencyrates/cryptocurrency"
	"github.com/bfg-dev/crypto-core/pkg/services/cyclicaddress"
	cyclicAddressPostgres "github.com/bfg-dev/crypto-core/pkg/services/cyclicaddress/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/email"
	"github.com/bfg-dev/crypto-core/pkg/services/ethaddress"
	userEthAddressPostgres "github.com/bfg-dev/crypto-core/pkg/services/ethaddress/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/mixpanel"
	"github.com/bfg-dev/crypto-core/pkg/services/spotware"
	spotwareAmqp "github.com/bfg-dev/crypto-core/pkg/services/spotware/amqp"
	spotwareSettingsRepository "github.com/bfg-dev/crypto-core/pkg/services/spotware/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/storage"
	"github.com/bfg-dev/crypto-core/pkg/services/tasks"
	transitOracleAmqp "github.com/bfg-dev/crypto-core/pkg/services/transit/amqp"
	"github.com/bfg-dev/crypto-core/pkg/services/users/countryperson"
	usersPostgres "github.com/bfg-dev/crypto-core/pkg/services/users/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/verification"
	verificationPostgres "github.com/bfg-dev/crypto-core/pkg/services/verification/postgres"
	assetsService "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/assets"
	assetsRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/assets/postgres"
	vbBalance "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance"
	vbBalanceRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance/postgres"
	balanceOperationResultRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balanceoperation/postgres"
	exchangeResultRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/exchangeresult/postgres"
	operationsRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operations/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operationsreader"
	operationsReaderRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operationsreader/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/orders"
	ordersRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/orders/postgres"
	pauseRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/pause/postgres"
	vbQueue "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/queue"
	virtualBalanceService "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/service"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions"
	transactionsRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions/postgres"
	"github.com/bfg-dev/crypto-core/pkg/types/exchange"
	"github.com/bfg-dev/crypto-core/pkg/types/merk"
	blockchainHelper "github.com/bfg-dev/crypto-helpers/pkg/services/blockchain"
	"github.com/bfg-dev/crypto-helpers/pkg/services/blockchain/abi"
	"github.com/bfg-dev/crypto-helpers/pkg/services/blockchain/abi/providers"
	"github.com/codegangsta/negroni"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

// Vbsrv ...
func (c *Cmd) Vbsrv() {
	/**
	 *  GetConnection application & services
	 */
	app := services.Application()
	err := app.Init(config)
	cmd.DieIfError(err, "app init error", app.Logger())

	//AuthManager
	authManager, err := api.NewSessionManager(app.SessionsStorage())
	cmd.DieIfError(err, "authManager manager init error", app.Logger())

	//DbProvider
	dbConnection := app.DBConnection()
	if dbConnection == nil {
		cmd.Die("unable to get db connection from application", app.Logger())
	}

	err = dbConnection.Ping()
	cmd.DieIfError(err, "unable to ping database connection", app.Logger())

	//Users repository
	usersRepository, err := usersPostgres.NewUserRepository(dbConnection)
	cmd.DieIfError(err, "user repository init error", app.Logger())

	//TempStorage service
	redisPool := app.RedisPool()
	tempStorageService, err := storage.NewRedisTempStoreService(redisPool)
	cmd.DieIfError(err, "tempStorage service init error", app.Logger())

	httpClient := &http.Client{
		Timeout: time.Second * 45,
	}

	amqpConnectionProvider, err := servicesAmqp.NewConnectionProvider(app.Config().GetString("CRYPTO_AMQP_HOST"),
		app.Config().GetInt("CRYPTO_AMQP_PORT"),
		app.Config().GetString("CRYPTO_AMQP_USER"),
		app.Config().GetString("CRYPTO_AMQP_PASS"),
		app.Config().GetString("CRYPTO_AMQP_VHOST"),
		app.Logger())
	cmd.DieIfError(err, "amqpConnectionProvider init error", app.Logger())

	amqpURL, err := platform.BuildAMQPConnectionUrl(app.Config().GetString("CRYPTO_AMQP_HOST"),
		app.Config().GetInt("CRYPTO_AMQP_PORT"),
		app.Config().GetString("CRYPTO_AMQP_USER"),
		app.Config().GetString("CRYPTO_AMQP_PASS"),
		app.Config().GetString("CRYPTO_AMQP_VHOST"))
	cmd.DieIfError(err, "amqpUrl build error", app.Logger())

	redisURL, err := platform.BuildRedisConnectionUrl(app.Config().GetString("CRYPTO_REDIS_HOST"),
		app.Config().GetInt("CRYPTO_REDIS_PORT"),
		app.Config().GetString("CRYPTO_REDIS_PASSWORD"))
	cmd.DieIfError(err, "redisURL build error", app.Logger())

	emailClient, err := email.NewClient(amqpConnectionProvider, exchange.EmailService, merk.EmailSend)
	cmd.DieIfError(err, "emailClient init error", app.Logger())

	baseURL := app.Config().GetString("CRYPTO_BASE_URL")
	emailService, err := email.NewService(baseURL, emailClient)
	cmd.DieIfError(err, "unable to create email service", app.Logger())

	currencyRateService, err := cryptocurrency.NewService(app.Config().GetString("CRYPTO_CURRENCY_URL"), tempStorageService, httpClient, app.Logger())
	cmd.DieIfError(err, "currencyRateService.NewRepo", app.Logger())

	exchangeResultRepo, err := exchangeResultRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "exchangeResultRepository.NewRepo", app.Logger())

	userSpotwareSettingsRepo, err := spotwareSettingsRepository.NewSpotwareUserOptionsRepo(dbConnection)
	cmd.DieIfError(err, "spotwareSettingsRepository.NewSpotwareUserOptionsRepo", app.Logger())

	spotwareClient, err := spotwareAmqp.NewClient(exchange.VirtualBalance, amqpConnectionProvider, app.Logger())
	cmd.DieIfError(err, "emailClient init error", app.Logger())

	cardsAmqpClient, err := cardsAqmp.NewCardsClient(exchange.VirtualBalance, amqpConnectionProvider, app.Logger())
	cmd.DieIfError(err, "cardsAqpCLient init error", app.Logger())

	transactionsRepository, err := transactionsRepo.NewRepo(dbConnection)
	cmd.DieIfError(err, "transitOracleAmqp init error", app.Logger())

	transitOracleClient, err := transitOracleAmqp.NewClient(exchange.Middleware, amqpConnectionProvider)
	cmd.DieIfError(err, "transitOracleAmqp init error", app.Logger())

	ethAddressTracker, err := blockchainAMQP.NewAddressTracker(exchange.Middleware, merk.EthAddressTrack, amqpConnectionProvider)
	cmd.DieIfError(err, "ethAddressTracker init error", app.Logger())

	cryptoAmqpUniq := app.Config().GetString("CRYPTO_AMQP_UNIQ")

	taskManager, err := tasks.NewCommonPlatformTaskManager(amqpURL, redisURL, cryptoAmqpUniq)
	cmd.DieIfError(err, "NewTaskManager init error", app.Logger())

	poolRegistry := cyclicaddress.NewPoolRegistry()

	bitGoClient, err := bitgo.NewClient(
		app.Config().GetString("CRYPTO_BITGOPROXY_URL"),
		app.Config().GetString("CRYPTO_BITGOPROXY_ACCESS_TOKEN"),
		app.Logger(),
	)
	cmd.DieIfError(err, "bitGoClient init error", app.Logger())

	bitGoAddressRepository, err := cyclicAddressPostgres.NewBitGoPoolRepository(dbConnection)
	cmd.DieIfError(err, "bitGoAddressRepository init error", app.Logger())

	bitGoPool, err := cyclicaddress.NewBitGoPool(bitGoClient, bitGoAddressRepository, taskManager, app.Logger())
	cmd.DieIfError(err, "bitGoPool init error", app.Logger())

	err = poolRegistry.Register(bitGoPool)
	cmd.DieIfError(err, "bitGoPool register error", app.Logger())

	ethAddressRepository, err := cyclicAddressPostgres.NewEthPoolRepository(dbConnection)
	cmd.DieIfError(err, "ethAddressRepository init error", app.Logger())

	ethAddressPool := cyclicaddress.NewEthPool(ethAddressRepository, taskManager, ethAddressTracker)
	err = poolRegistry.Register(ethAddressPool)
	cmd.DieIfError(err, "ethAddressPool register error", app.Logger())

	socketEventsProducer, err := broadcastqueue.NewProducer(app.Logger(), amqpConnectionProvider, broadcastqueue.SocketEventsKey)
	cmd.DieIfError(err, "socketEventsProducer init error", app.Logger())

	err = socketEventsProducer.Connect()
	cmd.DieIfError(err, "socketEventsProducer connect to queue error", app.Logger())

	balanceOperationResultRepo, err := balanceOperationResultRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "balanceOperationResultRepo.NewRepo", app.Logger())

	pauseRepo, err := pauseRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "pauseRepository init error", app.Logger())

	//BalanceRepo
	vbBalanceRepository, err := vbBalanceRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "vbBalanceRepository init error", app.Logger())

	//BalanceService
	vbBalanceService, err := vbBalance.NewService(vbBalanceRepository, currencyRateService, app.Logger())
	cmd.DieIfError(err, "vbBalanceService init error", app.Logger())

	mixpanelService, err := mixpanel.NewService(app.Config().GetString("MIXPANEL_ID"), app.Logger())
	cmd.DieIfError(err, "mixpanelService init error", app.Logger())

	operationsReaderRepository, err := operationsReaderRepo.NewRepo(dbConnection)
	cmd.DieIfError(err, "operationsReaderRepository init error", app.Logger())

	operationsReaderService, err := operationsreader.NewService(operationsReaderRepository, app.Logger())
	cmd.DieIfError(err, "operationsReaderService init error", app.Logger())

	spotwareAPIService, err := spotware.NewSpotwareApiService(
		app.Config().GetString("CRYPTO_SPOTWARE_PROXY_URL"),
		app.Config().GetString("CRYPTO_SPOTWARE_FIX_PROXY_URL"),
		spotwareClient,
		tempStorageService,
		app.Logger())
	cmd.DieIfError(err, "spotwareAPIService init error", app.Logger())

	//cardsRepository
	cardsRepository, err := cardsRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "card repository init error", app.Logger())

	//cardsHistoryRepository
	cardsHistoryRepository, err := cardsHistoryRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "card history repository init error", app.Logger())

	//ecp payment system credentials
	ecpCredentials := &ecp.Credentials{
		Host: app.Config().GetString("CRYPTO_ECP_API_HOST"),
		UID:  app.Config().GetString("CRYPTO_ECP_AUTH_UID"),
		PWD:  app.Config().GetString("CRYPTO_ECP_AUTH_PWD"),
		EID:  app.Config().GetString("CRYPTO_ECP_AUTH_EID"),
	}

	//ecp payment provider client
	ecpClient, err := ecp.NewClient(ecpCredentials, app.Logger())
	cmd.DieIfError(err, "ecp client init error", app.Logger())

	legacyAssetRepo, err := legacyAssetPostgres.NewAssetRepository(dbConnection)
	cmd.DieIfError(err, "assetRepo init error", app.Logger())

	legacyAssetService, err := legacyAssetService.NewService(legacyAssetRepo)
	cmd.DieIfError(err, "assetService init error", app.Logger())

	addressControlRepository, err := addressControlPostgres.NewRepository(dbConnection)
	cmd.DieIfError(err, "addressControlRepository init error", app.Logger())

	assetContractRepository, err := assetContractPostgres.NewRepository(dbConnection)
	cmd.DieIfError(err, "assetContractRepository init error", app.Logger())

	ethClient, err := ethclient.Dial(app.Config().GetString("ETH_RPC_URL"))
	cmd.DieIfError(err, "ethclient.Dial init error", app.Logger())

	provider, err := providers.NewHTTPProvider(app.Config().GetString("CRYPTO_DISCOVERY_URL"))
	cmd.DieIfError(err, "providers.NewHTTPProvider", app.Logger())

	getter, err := abi.NewGetter(provider)
	cmd.DieIfError(err, "abi.NewGetter", app.Logger())

	blockchainReaderService, err := blockchainHelper.NewReaderService(ethClient, getter)
	cmd.DieIfError(err, "blockchainReaderService init error", app.Logger())

	addressControlService, err := addresscontrol.NewService(addressControlRepository, app.Logger(), exchange.Middleware, amqpConnectionProvider, assetContractRepository, usersRepository)
	cmd.DieIfError(err, "addressControlService init error", app.Logger())

	//cards service
	cardsService, err := cards.NewService(
		ecpClient,
		cardsRepository,
		cardsHistoryRepository,
		app.Logger())
	cmd.DieIfError(err, "cardsService init error", app.Logger())

	assetsRepository, err := assetsRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "assetsRepository init error", app.Logger())

	vbAssetsService, err := assetsService.NewService(assetsRepository, app.Logger())
	cmd.DieIfError(err, "assetsService init error", app.Logger())

	bitgoTransactionsRepo, err := bitgo.NewTransactionRepository(dbConnection)
	cmd.DieIfError(err, "bitgo.NewTransactionRepository init error", app.Logger())

	vbQueueClient, err := vbQueue.NewClient(exchange.VirtualBalance, amqpConnectionProvider)
	cmd.DieIfError(err, "vbQueueClient init error", app.Logger())

	operationsRepository, err := operationsRepo.NewRepo(dbConnection)
	cmd.DieIfError(err, "transitOracleAmqp init error", app.Logger())

	vbService, err := virtualBalanceService.New(operationsRepository, vbQueueClient, app.Logger())
	cmd.DieIfError(err, "virtualBalanceService build error", app.Logger())

	builder, err := transactions.NewListBuilder(
		transactionsRepository,
		vbBalanceService,
		app.Config().GetString("CRYPTO_FRAUD_CONTROL_URL"),
		transitOracleClient,
		usersRepository,
		emailService,
		socketEventsProducer,
		exchangeResultRepo,
		userSpotwareSettingsRepo,
		spotwareClient,
		spotwareAPIService,
		balanceOperationResultRepo,
		pauseRepo,
		taskManager,
		mixpanelService,
		operationsReaderService,
		currencyRateService,
		cardsService,
		legacyAssetService,
		addressControlService,
		blockchainReaderService,
		assetContractRepository,
		vbAssetsService,
		app.Config().GetString("CRYPTO_VB_REPORT_EMAIL"),
		app.Config().GetString("VB_API_KEY"),
		bitGoClient,
		bitgoTransactionsRepo,
		vbService,
		app.Logger(),
	)
	cmd.DieIfError(err, "transactions.NewListBuilder", app.Logger())

	transactionsStrategy, err := transactions.NewStrategy(builder)
	cmd.DieIfError(err, "transactions.NewStrategy", app.Logger())

	//Task manager
	mgr, err := tasks.NewCommonPlatformTaskManager(amqpURL, redisURL, cryptoAmqpUniq)
	cmd.DieIfError(err, "NewTaskManager init error", app.Logger())

	//Verification repository
	verificationRepository, err := verificationPostgres.NewVerificationRepository(dbConnection)
	cmd.DieIfError(err, "verification repository init error", app.Logger())

	//Verification service
	verificationService, err := verification.NewService(app.Logger(), verificationRepository, mgr, baseURL)
	cmd.DieIfError(err, "verificationService init error", app.Logger())

	//countryPersonDetectorService
	countryPersonDetectorService, err := countryperson.NewCountryPersonDetectorService(verificationService)
	cmd.DieIfError(err, "countryPersonDetectorService init error", app.Logger())

	//UserEthAddress repository
	userEthRepo, err := userEthAddressPostgres.NewUserEthAddressRepository(dbConnection)
	cmd.DieIfError(err, "userEthRepo init error", app.Logger())

	//UserEthAddressAsset repository
	userEthAddressAssetRepo, err := userEthAddressPostgres.NewUserEthAddressAssetRepository(dbConnection)
	cmd.DieIfError(err, "userEthAddressAssetRepo init error", app.Logger())

	//BlacklistAddress repository
	blacklistRepo, err := userEthAddressPostgres.NewBlacklistRepository(dbConnection)
	cmd.DieIfError(err, "blacklistRepo init error", app.Logger())

	ethAddressService, err := ethaddress.NewService(blacklistRepo, userEthRepo, userEthAddressAssetRepo)
	cmd.DieIfError(err, "ethAddressService init error", app.Logger())

	//OrdersRepository repository
	ordersRepo, err := ordersRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "blacklistRepo init error", app.Logger())

	//orders service
	ordersService, err := orders.NewService(ordersRepo, app.Logger())
	cmd.DieIfError(err, "ordersService init error", app.Logger())

	//AssetHandler
	vbHandler, err := vbhandler.New(
		app,
		currencyRateService,
		operationsRepository,
		pauseRepo,
		transactionsStrategy,
		vbQueueClient,
		vbAssetsService,
		poolRegistry,
		vbBalanceService,
		legacyAssetService,
		countryPersonDetectorService,
		usersRepository,
		addressControlService,
		blockchainReaderService,
		cardsService,
		cardsAmqpClient,
		baseURL,
		spotwareAPIService,
		ethAddressService,
		ordersService,
	)
	cmd.DieIfError(err, "vbHandler init error", app.Logger())

	/**
	 *  GetConnection middleware
	 */

	//RequestId middleware
	requestIDMiddleware, err := middlewares.NewRequestID(app.Logger())
	cmd.DieIfError(err, "requestid middleware init error", app.Logger())

	//Error interceptor middleware
	errorInterceptorMiddleware, err := middlewares.NewErrorInterceptor(app.Logger())
	cmd.DieIfError(err, "interceptor middleware init error", app.Logger())

	//User provider middleware
	userProviderMiddleware, err := middlewares.NewUserProvider(usersRepository, app.Logger())
	cmd.DieIfError(err, "user provider middleware init error", app.Logger())

	//Only logged in middleware  middleware
	onlyLoggedInMiddleware, err := middlewares.NewOnlyLoggedIn(app.Logger())
	cmd.DieIfError(err, "only logged in middleware init error", app.Logger())

	onlyNotSuspendedMiddleware, err := middlewares.NewOnlyNotSuspended(app.Logger())
	cmd.DieIfError(err, "only not suspended in middleware init error", app.Logger())

	//Sessions middlware
	sessionsMiddleware, err := middlewares.NewSessions(authManager, app.Logger())
	cmd.DieIfError(err, "sessions middleware init error", app.Logger())

	//Only KYC passed middlware
	onlyKYCPassedMiddlware, err := middlewares.NewOnlyKYCPassed(app.Logger())
	cmd.DieIfError(err, "only kyc passed middleware init error", app.Logger())

	passwordRequiredMiddleware, err := middlewares.NewPasswordRequired(app.Logger())
	cmd.DieIfError(err, "password required middleware init error", app.Logger())

	//RestrictCountryPerson middleware
	restrictCountryPersonMiddleware, err := middlewares.NewRestrictCountryPerson(verificationService, countryPersonDetectorService, app.Logger())
	cmd.DieIfError(err, "restrictcountryperson middleware init error", app.Logger())

	optionalUserMiddleware, err := middlewares.NewOptionalUser(usersRepository, app.Logger())
	cmd.DieIfError(err, "optionalUserMiddleware init error", app.Logger())

	//Middleware for all routes
	//ORDER SENSITIVE!!!
	common := negroni.New(requestIDMiddleware, errorInterceptorMiddleware,
		sessionsMiddleware)
	guest := negroni.New(requestIDMiddleware, errorInterceptorMiddleware)

	loggedInMiddleware := common.With(negroni.Handler(onlyLoggedInMiddleware))

	r := mux.NewRouter()

	r.Handle("/assetsPrice", common.With(optionalUserMiddleware, negroni.Wrap(api.ResponseHandler(vbHandler.HandleAssetsPrice)))).Methods("GET")
	r.Handle("/volume24h", guest.With(negroni.Wrap(api.ResponseHandler(vbHandler.HandleVolume24h)))).Methods("GET")
	r.Handle("/tradings", common.With(optionalUserMiddleware, negroni.Wrap(api.ResponseHandler(vbHandler.HandleTradings)))).Methods("GET")
	r.Handle("/sparklines", common.With(optionalUserMiddleware, negroni.Wrap(api.ResponseHandler(vbHandler.HandleSparklines)))).Methods("GET")
	r.Handle("/volumes", common.With(optionalUserMiddleware, negroni.Wrap(api.ResponseHandler(vbHandler.HandleVolumes)))).Methods("GET")

	r.Handle("/deposit", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		restrictCountryPersonMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleDeposit)))).Methods("POST")

	r.Handle("/withdrawData", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleWithdrawData)))).Methods("POST")

	r.Handle("/withdraw", loggedInMiddleware.With(userProviderMiddleware, onlyKYCPassedMiddlware,
		onlyNotSuspendedMiddleware,
		restrictCountryPersonMiddleware,
		passwordRequiredMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleWithdraw)))).Methods("POST")

	r.Handle("/withdrawCancel", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		restrictCountryPersonMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleWithdrawCancel)))).Methods("POST")

	r.Handle("/sell", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleSell)))).Methods("POST")

	r.Handle("/buy", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleBuy)))).Methods("POST")

	r.Handle("/transactions", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleTransactions)))).Methods("POST")

	r.Handle("/balance", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleBalance)))).Methods("POST")

	r.Handle("/assets", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleAssets)))).Methods("GET")

	r.Handle("/symbols", loggedInMiddleware.With(
		userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleSymbols))),
	).Methods("GET")

	r.Handle("/assetByRouteKey", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleAssetByRouteKey)))).Methods("POST")

	r.Handle("/price", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandlePrice)))).Methods("POST")

	r.Handle("/createDepositForm", loggedInMiddleware.With(userProviderMiddleware,
		restrictCountryPersonMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleCreateDepositForm)))).Methods("POST")

	r.Handle("/formResponse", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleFormResponse)))).Methods("GET")

	r.Handle("/createWithdrawForm", loggedInMiddleware.With(userProviderMiddleware,
		onlyNotSuspendedMiddleware,
		restrictCountryPersonMiddleware,
		passwordRequiredMiddleware,
		negroni.Wrap(api.ResponseHandler(vbHandler.HandleCreateWithdrawForm)))).Methods("POST")

	//Logged in routes
	app.Logger().Error("ListenAndServe", zap.Error(http.ListenAndServe(":8091", r)))
}
