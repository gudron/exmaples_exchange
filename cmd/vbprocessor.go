package cmd

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/bfg-dev/crypto-core/pkg/helpers/cmd"
	"github.com/bfg-dev/crypto-core/pkg/helpers/platform"
	"github.com/bfg-dev/crypto-core/pkg/helpers/slices"
	bfgValidation "github.com/bfg-dev/crypto-core/pkg/helpers/validation"
	"github.com/bfg-dev/crypto-core/pkg/services"
	bfgAmqp "github.com/bfg-dev/crypto-core/pkg/services/amqp"
	"github.com/bfg-dev/crypto-core/pkg/services/asset"
	assetPostgres "github.com/bfg-dev/crypto-core/pkg/services/asset/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/bitgo"
	"github.com/bfg-dev/crypto-core/pkg/services/blockchain/addresscontrol"
	addressControlPostgres "github.com/bfg-dev/crypto-core/pkg/services/blockchain/addresscontrol/postgres"
	assetContractPostgres "github.com/bfg-dev/crypto-core/pkg/services/blockchain/assetcontract/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/broadcastqueue"
	"github.com/bfg-dev/crypto-core/pkg/services/cards"
	cardsAmqp "github.com/bfg-dev/crypto-core/pkg/services/cards/amqp"
	"github.com/bfg-dev/crypto-core/pkg/services/cards/ecp"
	cardsHistoryRepo "github.com/bfg-dev/crypto-core/pkg/services/cards/history/postgres"
	cardsRepo "github.com/bfg-dev/crypto-core/pkg/services/cards/postgres"
	cardTypes "github.com/bfg-dev/crypto-core/pkg/services/cards/types"
	"github.com/bfg-dev/crypto-core/pkg/services/currencyrates/cryptocurrency"
	"github.com/bfg-dev/crypto-core/pkg/services/email"
	"github.com/bfg-dev/crypto-core/pkg/services/mixpanel"
	"github.com/bfg-dev/crypto-core/pkg/services/spotware"
	spotwareAmqp "github.com/bfg-dev/crypto-core/pkg/services/spotware/amqp"
	spotwareSettingsRepository "github.com/bfg-dev/crypto-core/pkg/services/spotware/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/storage"
	"github.com/bfg-dev/crypto-core/pkg/services/tasks"
	transitOracleAmqp "github.com/bfg-dev/crypto-core/pkg/services/transit/amqp"
	usersPostgres "github.com/bfg-dev/crypto-core/pkg/services/users/postgres"
	vbAssetsService "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/assets"
	vbAssetsPostgres "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/assets/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance"
	vbBalance "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance"
	balanceRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance/postgres"
	vbBalanceRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balance/sync"
	balanceOperationResultRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/balanceoperation/postgres"
	exchangeResultRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/exchangeresult/postgres"
	operationsPg "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operations/postgres"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operationsreader"
	operationsReaderRepo "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/operationsreader/postgres"
	pauseRepository "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/pause/postgres"
	vbQueue "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/queue"
	virtualBalanceService "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/service"
	"github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions"
	transactionsPg "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/transactions/postgres"
	vbTypes "github.com/bfg-dev/crypto-core/pkg/services/virtualbalance/types"
	"github.com/bfg-dev/crypto-core/pkg/types"
	"github.com/bfg-dev/crypto-core/pkg/types/exchange"
	"github.com/bfg-dev/crypto-core/pkg/types/merk"
	blockchainHelper "github.com/bfg-dev/crypto-helpers/pkg/services/blockchain"
	"github.com/bfg-dev/crypto-helpers/pkg/services/blockchain/abi"
	"github.com/bfg-dev/crypto-helpers/pkg/services/blockchain/abi/providers"
	"github.com/ethereum/go-ethereum/ethclient"
	v "github.com/go-ozzo/ozzo-validation"
	"go.uber.org/zap"
)

// Vbprocessor ...
func (c *Cmd) Vbprocessor() {

	/**
	 *  Init application & services
	 */

	app := services.Application()
	err := app.Init(config)
	cmd.DieIfError(err, "app init error", app.Logger())

	//TempStorage service
	redisPool := app.RedisPool()
	tempStorageService, err := storage.NewRedisTempStoreService(redisPool)
	cmd.DieIfError(err, "tempStorage service init error", app.Logger())

	httpClient := &http.Client{
		Timeout: time.Second * 15,
	}

	currencyRateService, err := cryptocurrency.NewService(app.Config().GetString("CRYPTO_CURRENCY_URL"), tempStorageService, httpClient, app.Logger())
	cmd.DieIfError(err, "currencyRateService init error", app.Logger())

	// DbProvider
	dbConnection := app.DBConnection()
	if dbConnection == nil {
		cmd.Die("unable to get db connection from application", app.Logger())
	}

	err = dbConnection.Ping()
	cmd.DieIfError(err, "unable to ping database connection", app.Logger())

	enabledListenersStr := strings.TrimSpace(app.Config().GetString("CRYPTO_VB_ENABLED_LISTENERS"))

	app.Logger().Info("enabledListenersStr", zap.String("enabledListenersStr", enabledListenersStr))

	enabledListenersRaw := strings.Split(enabledListenersStr, ",")
	enabledListeners := make([]string, 0, 0)

	for _, enabledListenerRaw := range enabledListenersRaw {
		if len(enabledListenerRaw) == 0 {
			continue
		}
		enabledListeners = append(enabledListeners, strings.TrimSpace(enabledListenerRaw))
	}

	availableListenerNames := []string{
		vbTypes.OperationsPrepareListener,
		vbTypes.OperationsRollbackListener,
		vbTypes.OperationsCommitListener,
		vbTypes.ConvertUserAssetListener,
		cardTypes.FiatPaymentConfirmListener,
		vbTypes.BalanceSyncListener,
	}

	if len(enabledListeners) > 0 {
		validationError := v.Validate(enabledListeners, bfgValidation.ArrayIn(slices.StringToInterface(availableListenerNames)))

		if validationError != nil {
			app.Logger().Warn("Wrong listener names",
				zap.Strings("enabledListeners", enabledListeners),
				zap.String("availableListenerNames", strings.Join(availableListenerNames, ",")),
			)
		}
	} else {
		enabledListeners = availableListenerNames[:]
	}

	app.Logger().Info("Start virtual balance processor", zap.Strings("enabledListeners", enabledListeners))

	syncNotifyEmailStr := strings.TrimSpace(app.Config().GetString("CRYPTO_SYNC_NOTIFY_EMAILS"))
	app.Logger().Info("syncNotifyEmailStr", zap.String("syncNotifyEmailStr", syncNotifyEmailStr))

	syncNotifyEmails := strings.Split(syncNotifyEmailStr, ",")

	cryptoAmqpUniq := app.Config().GetString("CRYPTO_AMQP_UNIQ")

	amqpConnectionProvider, err := bfgAmqp.NewConnectionProvider(app.Config().GetString("CRYPTO_AMQP_HOST"),
		app.Config().GetInt("CRYPTO_AMQP_PORT"),
		app.Config().GetString("CRYPTO_AMQP_USER"),
		app.Config().GetString("CRYPTO_AMQP_PASS"),
		app.Config().GetString("CRYPTO_AMQP_VHOST"),
		app.Logger())
	cmd.DieIfError(err, "amqpConnectionProvider init error", app.Logger())

	amqpURL, err := platform.BuildAMQPConnectionUrl(app.Config().GetString("CRYPTO_AMQP_HOST"),
		app.Config().GetInt("CRYPTO_AMQP_PORT"),
		app.Config().GetString("CRYPTO_AMQP_USER"),
		app.Config().GetString("CRYPTO_AMQP_PASS"),
		app.Config().GetString("CRYPTO_AMQP_VHOST"))
	cmd.DieIfError(err, "amqpUrl build error", app.Logger())

	redisURL, err := platform.BuildRedisConnectionUrl(app.Config().GetString("CRYPTO_REDIS_HOST"),
		app.Config().GetInt("CRYPTO_REDIS_PORT"),
		app.Config().GetString("CRYPTO_REDIS_PASSWORD"))
	cmd.DieIfError(err, "redisURL build error", app.Logger())

	transactionsRepository, err := transactionsPg.NewRepo(app.DBConnection())
	cmd.DieIfError(err, "transactionsRepository init error", app.Logger())

	//operationsRepo
	operationsRepository, err := operationsPg.NewRepo(app.DBConnection())
	cmd.DieIfError(err, "operationsRepository init error", app.Logger())

	transitOracleClient, err := transitOracleAmqp.NewClient(exchange.Middleware, amqpConnectionProvider)
	cmd.DieIfError(err, "transitOracleAmqp init error", app.Logger())

	vbQueueClient, err := vbQueue.NewClient(exchange.VirtualBalance, amqpConnectionProvider)
	cmd.DieIfError(err, "vbQueueClient init error", app.Logger())

	usersRepository, err := usersPostgres.NewUserRepository(dbConnection)
	cmd.DieIfError(err, "user repository init error", app.Logger())

	emailClient, err := email.NewClient(amqpConnectionProvider, exchange.EmailService, merk.EmailSend)
	cmd.DieIfError(err, "emailClient init error", app.Logger())

	baseURL := app.Config().GetString("CRYPTO_BASE_URL")
	emailService, err := email.NewService(baseURL, emailClient)
	cmd.DieIfError(err, "unable to create email service", app.Logger())

	socketEventsProducer, err := broadcastqueue.NewProducer(app.Logger(), amqpConnectionProvider, broadcastqueue.SocketEventsKey)
	cmd.DieIfError(err, "broadcastQueueProducer init error", app.Logger())

	err = socketEventsProducer.Connect()
	cmd.DieIfError(err, "socketEventsProducer connect to queue error", app.Logger())

	exchangeResultRepo, err := exchangeResultRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "exchangeResultRepository.NewRepo", app.Logger())

	userSpotwareSettingsRepo, err := spotwareSettingsRepository.NewSpotwareUserOptionsRepo(dbConnection)
	cmd.DieIfError(err, "spotwareSettingsRepository.NewSpotwareUserOptionsRepo", app.Logger())

	spotwareClient, err := spotwareAmqp.NewClient(exchange.VirtualBalance, amqpConnectionProvider, app.Logger())
	cmd.DieIfError(err, "emailClient init error", app.Logger())

	balanceOperationResultRepo, err := balanceOperationResultRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "balanceOperationResultRepo.NewRepo", app.Logger())

	taskManager, err := tasks.NewCommonPlatformTaskManager(amqpURL, redisURL, cryptoAmqpUniq)
	cmd.DieIfError(err, "NewTaskManager init error", app.Logger())

	pauseRepo, err := pauseRepository.NewRepo(dbConnection)
	cmd.DieIfError(err, "pauseRepository init error", app.Logger())

	balanceRepo, err := balanceRepository.NewRepository(dbConnection)
	cmd.DieIfError(err, "balanceRepo init error", app.Logger())

	balanceService, err := balance.NewService(balanceRepo, currencyRateService, app.Logger())
	cmd.DieIfError(err, "balanceService init error", app.Logger())

	mixpanelService, err := mixpanel.NewService(app.Config().GetString("MIXPANEL_ID"), app.Logger())
	cmd.DieIfError(err, "mixpanelService init error", app.Logger())

	//BalanceRepo
	vbBalanceRepository, err := vbBalanceRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "vbBalanceRepository init error", app.Logger())

	//BalanceService
	vbBalanceService, err := vbBalance.NewService(vbBalanceRepository, currencyRateService, app.Logger())
	cmd.DieIfError(err, "vbBalanceService init error", app.Logger())

	operationsReaderRepository, err := operationsReaderRepo.NewRepo(dbConnection)
	cmd.DieIfError(err, "operationsReaderRepository init error", app.Logger())

	operationsReaderService, err := operationsreader.NewService(operationsReaderRepository, app.Logger())
	cmd.DieIfError(err, "operationsReaderService init error", app.Logger())

	spotwareAPIService, err := spotware.NewSpotwareApiService(
		app.Config().GetString("CRYPTO_SPOTWARE_PROXY_URL"),
		app.Config().GetString("CRYPTO_SPOTWARE_FIX_PROXY_URL"),
		spotwareClient,
		tempStorageService,
		app.Logger())
	cmd.DieIfError(err, "spotwareAPIService init error", app.Logger())

	//cardsRepository
	cardsRepository, err := cardsRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "card repository init error", app.Logger())

	//cardsHistoryRepository
	cardsHistoryRepository, err := cardsHistoryRepo.NewRepository(dbConnection)
	cmd.DieIfError(err, "card history repository init error", app.Logger())

	//ecp payment system credentials
	ecpCredentials := &ecp.Credentials{
		Host: app.Config().GetString("CRYPTO_ECP_API_HOST"),
		UID:  app.Config().GetString("CRYPTO_ECP_AUTH_UID"),
		PWD:  app.Config().GetString("CRYPTO_ECP_AUTH_PWD"),
		EID:  app.Config().GetString("CRYPTO_ECP_AUTH_EID"),
	}

	//ecp payment provider client
	ecpClient, err := ecp.NewClient(ecpCredentials, app.Logger())
	cmd.DieIfError(err, "ecp client init error", app.Logger())

	//cards service
	cardsService, err := cards.NewService(
		ecpClient,
		cardsRepository,
		cardsHistoryRepository,
		app.Logger())

	cmd.DieIfError(err, "cardsService init error", app.Logger())

	assetRepo, err := assetPostgres.NewAssetRepository(dbConnection)
	cmd.DieIfError(err, "assetRepo init error", app.Logger())

	assetService, err := asset.NewService(assetRepo)
	cmd.DieIfError(err, "assetService init error", app.Logger())

	addressControlRepository, err := addressControlPostgres.NewRepository(dbConnection)
	cmd.DieIfError(err, "addressControlRepository init error", app.Logger())

	assetContractRepository, err := assetContractPostgres.NewRepository(dbConnection)
	cmd.DieIfError(err, "addressControlRepository init error", app.Logger())

	addressControlService, err := addresscontrol.NewService(addressControlRepository, app.Logger(), exchange.Middleware, amqpConnectionProvider, assetContractRepository, usersRepository)
	cmd.DieIfError(err, "addressControlService init error", app.Logger())

	ethClient, err := ethclient.Dial(app.Config().GetString("ETH_RPC_URL"))
	cmd.DieIfError(err, "ethclient.Dial init error", app.Logger())

	provider, err := providers.NewHTTPProvider(app.Config().GetString("CRYPTO_DISCOVERY_URL"))
	cmd.DieIfError(err, "providers.NewHTTPProvider", app.Logger())

	getter, err := abi.NewGetter(provider)
	cmd.DieIfError(err, "abi.NewGetter", app.Logger())

	blockchainReaderService, err := blockchainHelper.NewReaderService(ethClient, getter)
	cmd.DieIfError(err, "blockchainReaderService init error", app.Logger())

	vbAssetsRepository, err := vbAssetsPostgres.NewRepository(dbConnection)
	cmd.DieIfError(err, "vbAssetsRepository init error", app.Logger())

	vbAssetService, err := vbAssetsService.NewService(vbAssetsRepository, app.Logger())
	cmd.DieIfError(err, "assetService init error", app.Logger())

	bitGoClient, err := bitgo.NewClient(
		app.Config().GetString("CRYPTO_BITGOPROXY_URL"),
		app.Config().GetString("CRYPTO_BITGOPROXY_ACCESS_TOKEN"),
		app.Logger(),
	)
	cmd.DieIfError(err, "bitGoClient init error", app.Logger())

	bitgoTransactionsRepo, err := bitgo.NewTransactionRepository(dbConnection)
	cmd.DieIfError(err, "bitgo.NewTransactionRepository init error", app.Logger())

	vbService, err := virtualBalanceService.New(operationsRepository, vbQueueClient, app.Logger())
	cmd.DieIfError(err, "virtualBalanceService build error", app.Logger())

	operationsListBuilder, err := transactions.NewListBuilder(
		transactionsRepository,
		balanceService,
		app.Config().GetString("CRYPTO_FRAUD_CONTROL_URL"),
		transitOracleClient,
		usersRepository,
		emailService,
		socketEventsProducer,
		exchangeResultRepo,
		userSpotwareSettingsRepo,
		spotwareClient,
		spotwareAPIService,
		balanceOperationResultRepo,
		pauseRepo,
		taskManager,
		mixpanelService,
		operationsReaderService,
		currencyRateService,
		cardsService,
		assetService,
		addressControlService,
		blockchainReaderService,
		assetContractRepository,
		vbAssetService,
		app.Config().GetString("CRYPTO_VB_REPORT_EMAIL"),
		app.Config().GetString("VB_API_KEY"),
		bitGoClient,
		bitgoTransactionsRepo,
		vbService,
		app.Logger())
	cmd.DieIfError(err, "operationsListBuilder init error", app.Logger())

	transactionsStrategy, err := transactions.NewStrategy(operationsListBuilder)
	cmd.DieIfError(err, "transactionsStrategy init error", app.Logger())

	operationHandlers, err := vbQueue.NewOperationHandlers(transactionsStrategy, operationsRepository, balanceService, app.Logger())
	cmd.DieIfError(err, "operationHandlers init error", app.Logger())

	balanceSync, err := sync.NewSync(transactionsStrategy, operationsRepository, balanceService, vbQueueClient, spotwareAPIService, emailService, syncNotifyEmails, app.Logger())
	cmd.DieIfError(err, "balanceSync init error", app.Logger())

	convertUserAssetHandler, err := vbQueue.NewConvertUserAssetHandler(transactionsStrategy, operationsRepository, balanceService, vbQueueClient, app.Logger())
	cmd.DieIfError(err, "convertUserAssetHandler init error", app.Logger())

	//fiatConfirm handler
	fiatPaymentConfirmHandler, err := cardsAmqp.NewFiatPaymentConfirmHandler(
		operationsRepository,
		transactionsStrategy,
		vbBalanceService,
		vbQueueClient,
		cardsService,
		vbAssetService,
		app.Logger())
	cmd.DieIfError(err, "fiatPaymentConfirmHandler init error", app.Logger())

	//balanceSyncHandler handler
	balanceSyncHandler, err := vbQueue.NewBalanceSyncHandler(balanceSync, balanceService, userSpotwareSettingsRepo, usersRepository, app.Logger())
	cmd.DieIfError(err, "balanceSyncHandler init error", app.Logger())

	quitCh := make(<-chan bool)

	if slices.InStrArray(vbTypes.OperationsPrepareListener, enabledListeners) {
		operationPrepareListener, err := vbQueue.NewOperationPrepareListener(exchange.VirtualBalance,
			merk.VbOperationPrepare,
			vbTypes.OperationsPrepareQueue,
			amqpConnectionProvider,
			defaultConcurrency,
			defaultMaxRetriesCount,
			cryptoAmqpUniq,
			vbQueueClient,
			app.Logger())
		cmd.DieIfError(err, "operationPrepareListener init error", app.Logger())

		err = operationPrepareListener.Listen(operationHandlers.(vbQueue.OperationHandlers))
		cmd.DieIfError(err, "operationPrepareListener listen error", app.Logger())
	}

	//rollback operation Listener
	if slices.InStrArray(vbTypes.OperationsRollbackListener, enabledListeners) {
		operationPrepareListener, err := vbQueue.NewOperationRollbackListener(exchange.VirtualBalance,
			merk.VbOperationRollback,
			vbTypes.OperationsRollbackQueue,
			amqpConnectionProvider,
			defaultConcurrency,
			defaultMaxRetriesCount,
			cryptoAmqpUniq,
			app.Logger())
		cmd.DieIfError(err, "operationRollbackListener init error", app.Logger())

		err = operationPrepareListener.Listen(operationHandlers.(vbQueue.OperationHandlers))
		cmd.DieIfError(err, "operationRollbackListener listen error", app.Logger())
	}

	if slices.InStrArray(vbTypes.OperationsCommitListener, enabledListeners) {
		operationPrepareListener, err := vbQueue.NewOperationCommitListener(exchange.VirtualBalance,
			merk.VbOperationCommit,
			vbTypes.OperationsCommitQueue,
			amqpConnectionProvider,
			defaultConcurrency,
			defaultMaxRetriesCount,
			cryptoAmqpUniq,
			app.Logger())
		cmd.DieIfError(err, "operationCommitListener init error", app.Logger())

		err = operationPrepareListener.Listen(operationHandlers.(vbQueue.OperationHandlers))
		cmd.DieIfError(err, "operationCommitListener listen error", app.Logger())
	}

	//FiatPaymentsConfirmation
	if slices.InStrArray(cardTypes.FiatPaymentConfirmListener, enabledListeners) {
		fiatPaymentConfirmListener, err := cardsAmqp.NewFiatPaymentConfirmListener(exchange.VirtualBalance,
			merk.VbFiatPaymentsConfirmation,
			cardTypes.PaymentConfirmationQueue,
			amqpConnectionProvider,
			defaultConcurrency,
			defaultMaxRetriesCount,
			cryptoAmqpUniq,
			app.Logger())
		cmd.DieIfError(err, "fiatPaymentConfirmListener init error", app.Logger())

		err = fiatPaymentConfirmListener.Listen(fiatPaymentConfirmHandler.(cards.FiatPaymentConfirmHandler))
		cmd.DieIfError(err, "fiatPaymentConfirmListener listen error", app.Logger())
	}

	//BalanceSyncListener
	if slices.InStrArray(vbTypes.BalanceSyncListener, enabledListeners) {
		balanceSyncListener, err := vbQueue.NewBalanceSyncListener(
			exchange.VirtualBalance,
			merk.VbBalanceSync,
			vbTypes.BalanceSyncQueue,
			amqpConnectionProvider,
			defaultConcurrency,
			defaultMaxRetriesCount,
			cryptoAmqpUniq,
			vbQueueClient,
			app.Logger())
		cmd.DieIfError(err, "balanceSyncListener init error", app.Logger())

		err = balanceSyncListener.Listen(balanceSyncHandler.(vbQueue.BalanceSyncHandler))
		cmd.DieIfError(err, "balanceSyncListener listen error", app.Logger())
	}

	//ConvertUserAssetListener
	if slices.InStrArray(vbTypes.ConvertUserAssetListener, enabledListeners) {
		convertUserAssetListener, err := vbQueue.NewConvertUserAssetListener(exchange.VirtualBalance,
			merk.VbConvertUserAsset,
			vbTypes.ConvertUserAssetQueue,
			amqpConnectionProvider,
			defaultConcurrency,
			defaultMaxRetriesCount,
			cryptoAmqpUniq,
			vbQueueClient,
			app.Logger())
		cmd.DieIfError(err, "convertUserAssetListener init error", app.Logger())

		err = convertUserAssetListener.Listen(convertUserAssetHandler.(vbQueue.ConvertUserAssetHandler))
		cmd.DieIfError(err, "convertUserAssetListener listen error", app.Logger())
	}

	<-quitCh

	app.Logger().Info("All workers exited", zap.Error(err))
}

func getBitGoWalletsPasswords() map[types.Currency]string {
	envPrefix := "BITGO_WITHDRAW_PASSWORD_"
	addresses := make(map[types.Currency]string)

	for _, env := range os.Environ() {
		v := strings.Split(env, "=")
		if strings.Contains(v[0], envPrefix) {
			key := strings.ToLower(v[0][len(envPrefix):])

			addresses[types.Currency(key)] = v[1]
		}
	}

	return addresses
}
