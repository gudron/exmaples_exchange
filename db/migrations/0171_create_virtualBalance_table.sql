-- +goose Up
CREATE TYPE public."virtualBalanceAssets" AS ENUM (
'USD', 'EUR', 'GBP', 'BTC', 'ETH',
'LTC', 'BCH', 'XRP', 'BTG', 'BMxiETH',
'BMxTMR', 'BMxVTP', 'BMx1DV', 'BMxDDR',
'BMxBN20', 'BMxT20', 'BMxSPY', 'USDT');

CREATE TABLE public."virtualBalances"
(
    id SERIAL,
    "createdAt" timestamp with time zone NOT NULL DEFAULT current_timestamp,
    "updatedAt" timestamp with time zone NOT NULL DEFAULT current_timestamp,
    "userId" bigint NOT NULL,
    "asset" "virtualBalanceAssets" NOT NULL,
    "operationId" bigint NOT NULL,
    "amount" decimal(50,0) NOT NULL
);

CREATE UNIQUE INDEX  "virtualBalances_operationId_asset" ON "virtualBalances" ("operationId", "asset");
CREATE INDEX  "virtualBalances_userId" ON "virtualBalances" ("userId");
-- +goose Down

DROP TABLE "virtualBalances";
DROP TYPE public."virtualBalanceAssets";
